#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#print("Content-Type: text/html\n\nDUPA")

__version__ = '202102171210'

'''
     Licencja ISC (ISCL)

     Prawo autorskie (c) 2010-2015. Leszek Loboda - Zakład Elektroniczny Letronik.

     Uprawnienie do używania, kopiowania, modyfikowania i / lub rozpowszechniania tego programu w dowolnym celu, 
     z lub bez opłat zostaje przyznany, pod warunkiem, że powyższe informacje o prawach autorskich i niniejsze 
     pozwolenie pojawią się na wszystkich kopiach. 
     OPROGRAMOWANIE JEST DOSTARCZANE "TAK JAK JEST" I AUTOR NIE UDZIELA ŻADNYCH GWARANCJI W ODNIESIENIU DO 
     TEGO OPROGRAMOWANIA, W TYM DOMYŚLNYCH GWARANCJI PRZYDATNOŚCI HANDLOWEJ I ZDATNOŚCI. 
     W ŻADNYM WYPADKU AUTOR NIE PONOSI ODPOWIEDZIALNOŚCI ZA ŻADNE SZCZEGÓLNE, BEZPOŚREDNIE, 
     POŚREDNIE LUB WTÓRNE LUB JAKIEKOLWIEK SZKODY, W TYM WYNIKAJĄCE Z UTRATY DANYCH LUB ZYSKÓW, 
     CZY TO W RAMACH UMOWY, ZANIEDBANIA LUB INNYCH CZYNÓW NIEDOZWOLONYCH , WYNIKAJĄCE Z / LUB W ZWIĄZKU 
     Z UŻYCIEM /LUB DZIAŁANIEM TEGO OPROGRAMOWANIA.

     ISC License (ISCL)

     Copyright (c) 2010-2018. Leszek Loboda - Zakład Elektroniczny Letronik.

     Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee 
     is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
     THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
     SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. 
     IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, 
     OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
     DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
     ARISING OUT OF OR IN CONNECTION WITH THE USE

'''

'''
lvSu.py start 20180329
202006260850
python3
sid = int(cookie[service+'lvSu'].value)
201911050927
dodanie lvCalib.py
201809271649
    try:
        ftest=open('test.test','rw')
        ftest.write('test')
    except:
20180702_1512
    if loggLevel:
        try:
            flogg.close()
        except:
            pass
20180403
------------------
20180329
userTest("letronikVISITORS",sessionKey,userN,result,'su')
'''


loggLevel=0
import sys
import os
from datetime import datetime, timedelta
import lvConf
from lvCommonLib import greens, blues,sledz,printLogg2,regionParents2,pHelp,countersInRegion,dayTimes

workDir=os.path.dirname(sys.argv[0])
tmpDir=os.path.join(workDir,"tmp")
logName=os.path.join(tmpDir,'zelVC_lvSu.log')
flogg=open(logName,'w')#kasowanie loggu
flogg.close()
now = datetime.now()
timestamp=now.strftime("%Y%m%d %H:%M:%S ")
loggLevel=lvConf.loggLevel
logV=(__version__, loggLevel, timestamp,logName)
printLogg2(logV,0,"logV=%s" % str(logV))
#sys.path.append("py")
sys.path.append(os.path.join('..','conf'))
   
dirwin=os.path.join('./','pylibwin')
dirlin=os.path.join('./','pyliblin')
if os.name=='nt':
    if os.path.isdir(dirwin):
        sys.path.append(dirwin)
        #print "lib %s" % dirwin

else:
    if os.name=='posix':
        if os.path.isdir(dirlin):
            sys.path.append(dirlin)
            #print "lib %s" % dirlin
    else:
        print("Nierozpoznany system %s" % os.name)
        sys.exit(-1)

printLogg2(logV,0,os.name)
dirPy64=os.path.join('..','py64')
if os.path.isdir(dirPy64):
    sys.path.append(dirPy64)
#import pickle
#import shutil
import string
#import filecmp
#import ftplib #import FTP
#import socket
#from openpyxl import Workbook
import time
#import subprocess



import cgi
import cgitb; cgitb.enable()
import http.cookies
import lvSuSes





'''
Wymagane pakiety
sudo pip install openpyxl
__version__ = '20180224_1143'
dirPy64int=os.path.join('..','py64int')
if os.path.isdir(dirPy64int):
    sys.path.append(dirPy64int)
'''

#

#__version__ = 'A141001-02'
#def sledz(loggLevel, level,tresc):
#def pHelp(level,tresc):
#def dayTimes(st,ft):
#def login(form, sess):
#def cycleH(day,openH,closeH):
#def byMonths2(sy,sm,sdd, shh, shm,fy,fm,fdd, fhh, fhm):
#def byMonths(sd,fd):
#def counterTab(configNameV):
#def regionCounters(region,level,xl=0):
#def countersInRegion(region,cur):
#def regionChildren(region):
#def regionParents():
#def regionParents1():
#
#mtcbF -----------Malowanie tablicy check-box funkcja  197
#mtdcb malowanie tablicy dni
#mtg-----------------malowanie tablicy godzin start
#START###########################         START START START START START START START START START START START START
#wdzf------------wczytanie danych z formularza
#wcko--------------wyciąganie czasów końca okresu
#wcpo--------------wyciąganie czasów początku okresu
#ccbr wczytywanie chechboxow regionow  449
#oswr odczytywanie starego wyboru regionów
#ort------ odczytywanie regionTree
#wrn wczytywanie radio nawigacji 475
#wddscb wyznaczanie nazw dni do sprawdzania checkboxów  -sld
sld=1
wkl=1
tto=1
#dayListM wykaz miesięcy dni do analizy 541
#wtmda ------------------ wybieranie tablic miesięcznych do analizy
#wligda------------------WYBIERANIE LICZNIKÓW I GRUP DO ANALIZY, obliczanie tablic obiektów 1017
#regRow=[(parentList[nazwa[0]],nazwa[0]),nazwa[1],0,0,0,0,False]
#kolorDict={} - słownik kolorów argument - nazwa rebiektu

#ddwd ---------   dane do wykresu dziennego start 658
#ddtg ----------  dane do tablicy godzinowej
#wynaw wyświetlanie nawigacji 882


'''
20180308_1145
20180306_1834
dirwin=os.path.join('..','pylibwin')
dirlin=os.path.join('..','pyliblin')
if os.name=='nt':
'''

#import lvCf #plik konfiguracji
#import leSe

#sys.path.append(os.path.dirname(os.environ['SCRIPT_FILENAME']))
#konfiguracja

'''hostDB="localhost"
userDB="12398781_dc"
pwdDB="eensosfo72"
dbDB="12398781_dc"
portDB=3306'''

helpLevel=0
skrypt=os.path.basename(sys.argv[0])
scriptPath=os.path.dirname(sys.argv[0])
keyDir=os.path.join(scriptPath,'keyDir')
#print keyDir
firmName=''
firmCode=''
vl=()
chartNo=0
sys.stderr = sys.stdout
#print "Content-Type: text/html\n\n"





printLogg2(logV)

def variableSave(variable, fileName):
    try:
        fn=open(fileName,'wb')
    except:
        return "cant open %s" % fileName
    fn.write(variable)
    fn.close
    return 0

def userTest(title,session,user,userFirm,firmCode):
    '''sprawdza, czy użykownik ma uprawnienia.'''

    if userFirm!=firmCode:#userFirm pobrany z 'sekretu' firmCode z pliku lvCf.py
        print('''<!-- start(title,session,user='',firm='')-->\n''')
        print("<h1>%s</h1>" % title)
        print('''<p style="font-size: 12px;">Skrypt %s wersja %s</p>'''% (script,__version__))        
        print('''<h3>!!!Nie masz uprawnień do -%s- masz uprawnienia do -%s-!!!</h3>''' % (firmCode,userFirm))
        footer()
        sys.exit(0)

def footer():
    '''wstawia zamknięcie strony'''
    print('''<!-- footer()-->\n''')
    print("<h2> Program wykonał się do końca. </h2>")
    print("</body></html>")        

def dayTimesOld(st,ft):
    '''listę krotek dziennych (yymm,ddhhMM,ddhhMM) - '''
    timesLs=[]


    item=(st.strftime('%y%m'),[])
    if st.day==ft.day and st.month==ft.month and st.year==ft.year:
        if ft.strftime('%H%M')=='2359':
            finish=ft.strftime('%d2400')
        else:
            finish=ft.strftime('%d%H%M')
        timesLs.append((st.strftime('%y%m'),st.strftime('%d%H%M'),finish))
    else:
        bt=st+timedelta(1)
        #print bt
        timesLs.append((st.strftime('%y%m'),st.strftime('%d%H%M'),st.strftime('%d2400')))                
        while bt<ft.replace(hour=0, minute=0,second=0,microsecond=0):
            timesLs.append((bt.strftime('%y%m'),bt.strftime('%d0000'),bt.strftime('%d2400')))
            bt+=timedelta(1)
        if ft.strftime('%H%M')=='2359':
            finish=ft.strftime('%d2400')
        else:
            finish=ft.strftime('%d%H%M')
        timesLs.append((ft.strftime('%y%m'),ft.strftime('%d0000'),finish))            

                
    return timesLs



               



def counterTab(configNameV):
    '''wczytywanie pliku konfiguracji - wykazu liczników
ścieżek dostępu, opisów
zamiana na listę '''
    config=[[],False]
    #print configNameV,"<br>"
    
    
    try:
         fconf=open(configNameV,'r')
    except:
         config[1]="can't open file <br>"+configNameV
         #printLogg2(0, "Error %s:" % e.args[0])
         return config
    

    lines=fconf.readlines()
    #print lines
    for line in lines:
        #print line
        line = string.strip(line)
        if (string.find(line,'#')== -1)&(len(line)>4):
            #printLogg2(4,line)
            xline=[]
            parametry=string.split(line,',')
            if len(parametry)<2:
                config[1]="config ERROR in '%s'" % line
                fconf.close()
                return config
            #printLog(4, parametry)
            for xitem in parametry:
                xline.append(xitem.strip())
            config[0].append(xline)


    #config['confError']=confError
    fconf.close()    
    return config

'''
Procedura regionCounters, rC.py, wyznacza regiony poległe i liczniki.
Powinna podawać też regiony nadrzędne?
Argumentem jest region i głebokość podgrupy.
Otrzymujemy listę regionów jako listy liczników.

'''
region=''
counterList={}
counterList[region]=[]
mapa=''
regions=region
printLogg2(logV,2,"region = %s" % str(region))

outStr=''
def regionCountersOld(region,level,xl=0):
    global cur,now, countList,mapa,regions, outStr
   
    #print region, xl
    if region[1]=='g':
        if xl==level:
            regions=region
            counterList[regions]=[]
        xl+=1
        tab=''
        for t in range(xl):
            tab+='    '
        command ="select child, sign from regions where parent='%s' order by child;" % region[0]
        outStr+= "\n<br> %s" % command
        cur.execute(command)
        lista=cur.fetchall()
        outStr+= "\n<br> %s %d %d" % (str(lista),level,xl)
        for x in lista:
            child=x[0]
            sign=x[1]        
            xmap= "%s%d %s %s" % (tab,xl,child,sign)
            #print xmap
            mapa+=xmap+'\n'
            if sign!='g':
                #print child,sign
                #countList.append((child,sign,level,xl))
                if xl>1:
                    counterList[regions].append((child,sign,region[0]))
                else:
                    counterList[(child,sign)]=[(child,sign,region[0])]
            else:
                
                regionCounters((child,sign),level,xl)
    else:
        command ="select parent from regions where child='%s' ;" % region[0]
        cur.execute(command)
        parentV=cur.fetchone()[0]
        #print "<br>Parent=%s, Region= %s" % (parentV,region[0])
        counterList[(region[0],region[1])]=[(region[0],region[1],region[0])]
    return outStr
#countersInRegion start
#counterLs - wykaz liczników w regione jest konieczne do rekurencji, musi zostać.
def countersInRegionTtrash(region):
    global cur,now, counterLs
    command ="select child, sign from regions where parent='%s';" % region
    #print command
    cur.execute(command)
    lista=cur.fetchall()
    #print lista
    for x in lista:
        child=x[0]
        sign=x[1]
        #print child, sign
        if sign!='g':
            counterLs.append((child,sign))
        else:
            countersInRegion(child)

#countersInRegion stop


def regionChildren(region):
    global cur#,now, countList
    #childList=[]
    command ="select child, sign from regions where parent='%s';" % region
    #print command
    cur.execute(command)
    lista=cur.fetchall()
    return list(lista)
'''
    for x in lista:
        countList=[]
        child=x[0]
        sign=x[1]
        print child, sign
        #if sign!='g':
        countList.append((child,sign))
        #else:
            
            
            #regionCounters(child)
        childList.append((child,countList))
    return childList
'''
#firmName='Firma'        
#parentList={firmName:''}                
def regionParents():
    global counterList,parentList
    xlist=list(counterList.keys())
    for x in xlist:
        command ="select parent from regions where child='%s' ;" % x[0]
        cur.execute(command)
        parentV=cur.fetchone()
        if parentV:   
            parentList[x[0]]=parentV[0]
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''

    return xlist
        
def regionParents1():
    global regionLs,parentList, cur
    for x in regionLs:
        command ="select parent from regions where child='%s' ;" % x[0]
        cur.execute(command)
        parentV=cur.fetchone()
        if parentV:   
            parentList[x[0]]=parentV[0]
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''

    #return xlist

def regionParents2trash(lista,parentList, cur):
    #global parentList, cur
    for x in lista:
        printLogg2(logV,1, "21. regionParents2(lista)")
        command ="select parent from regions where child='%s' and sign='%s';" % (x[0],x[1])
        printLogg2(logV,1, command)
        cur.execute(command)
        parentV=cur.fetchone()
        printLogg2(logV,1, "22. parentV = %s" % str(parentV))
        #print parentV
        #sys.exit(1)
        if parentV:   
            parentList[x[0]]=parentV[0]
            printLogg2(logV,1,parentList[x[0]])
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''




        

#mtg-----------------malowanie tablicy godzin stop
    
#def core(form,lvCf, userN,firmName,firmCode,direction,result,ts,cookie,sessionKey,service,vl):
def core(logV,form,lvCf,leSe, userN,result,ts,cookie,sessionKey,service,vl):
    
    
#START###########################         START START START START START START START START START START START START
    #firmDir=lvCf.firmDir
    
    firmName=lvCf.firmName
    global cur, counterLs, helpLevel, kolorDict
    global counterList,parentList
    global sld, wkl, tto
    #printLogg2(logV,1,"firmDir = %s firmName=%s" % (firmDir,firmName))
    try:
        headerName=leSe.headerName
        siteTitle=leSe.siteTitle
    except:
        headerName=service+'_'
        siteTitle=service+'_'

    
    #loggLevel=9
    #lvSuSes.display_sess(userN,cookie,sessionKey,firmName,"Przeglądarka odwiedzalności")
    lvSuSes.display_sess(userN,service,cookie,sessionKey,ts,result,siteTitle)
    userTest("letronikVISITORS",sessionKey,userN,result,'su')    
    #lvSuSes.data['logged_until'] = time.time() + 10*60 #po 60 sekundach sesja jest przerywana
    #lvSuSes.cookie
    #lvSuSes.data.setdefault('output', [])
    printLogg2(logV,1, "userTest OK")
    regionToCheck =''# do przekazania w polu ukrytym do sprawdzania chechBoxów

    #---------aktualizacja daty  
    now = datetime.now()
    #--------godziny do zapytania sql

    #fileNumber=0
    #lastFile='noFile'
    #----------data do logu
    timestamp=now.strftime("%Y%m%d %H:%M:%S ")

    #dayName=now.strftime("%Y-%m-%d")

    #SfirmTrashCsvDir="../letronik/csvTrash"

    #fname=os.path.join(leSe.dirPrefix,lvCf.fname)


    #wdzf------------wczytanie danych z formularza
    #form = cgi.FieldStorage()
    if form.getvalue('debLev'):
        #print form.getvalue('debLev')
        lL = int(form.getvalue('debLev'))
        logV=(logV[0],lL,logV[2])
        printLogg2(logV,1, "debLev=%d"% logV[1])
    if form.getvalue('helpLev'):
        helpLevel = int(form.getvalue('helpLev'))
        printLogg2(logV,1, "heplLev=%d"% helpLevel)
   
      
       
    #----wczytywanie firmName   
    if form.getvalue('firmSym'):
       firmName = form.getvalue('firmSym')
    region=(firmName,'g')
    parentList={firmName:''}
    #print "Content-Type: text/html\n\n"
    #print
    '''<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html><head><meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <title>letronikVISITORS config for: %s.</title>
      <meta content="Letronik" name="author">
    <STYLE>
    <!--
      tr 
      .initial { background-color: #ffffff; color:#000000 }
      .normal { background-color: #ffffff; }
      .highlight { background-color: #8888FF }
      .red { background-color: #FFC0CB}
    //-->
    </style>
    </head><body>''' % firmName
          
    print("<p><font style=\"font-size: 10px;\">letronik </font>")
    print("<font style=\"font-size: 20px;\">VISITORSconfig</font>")
    print("<font style=\"font-size: 14px;\"><sup>%s</sup></font>" % __version__)
    print("<font style=\"font-size: 10px;\">&nbsp;&nbsp;dla firmy:</font>") 
    print("<font style=\"font-size: 15px;\">%s</font>"  % headerName)
    print("""<h1>Kofigurator generatora raportów <a href="lv.py?service=%s">letronikVISITORS</a></h1>""" % service) 
    print("""<a href="lvDbData.py?service=%s"><h2>Konfiguracja parametrów dostępowych bazy danych</h2></a>""" % service)    
    print("""<a href="lvMap.py?service=%s"><h2>Mapa sieci handlowej</h2></a>""" % service)
    print("""<a href="lvUsers.py?service=%s"><h2>Konfiguracja użytkowników</h2></a>""" % service)
    print("""<a href="lvCalib.py?service=%s"><h2>Konfiguracja współczynników kalibracji</h2></a>""" % service)    
    print("""<p>Powrót do <a href="lv.py?service=%s">letronikVISITORS</a></p>""" % service)  
    try:
        ftest=open('test.test','w')
        ftest.write('test')
    except:
        #system ro
        print("<br>system RO")
        variableSave('now', '/tmp/zelVC_configNow.txt')#blokowanie szukania sieci jeżeli konfiguracja
        variableSave('now', '/tmp/zelVC_confInstall.run')#szybka konfiguracja
    else:
        ftest.close()
        print("<br>system RW")
    print("</body></html>")
    #jeżeli system RO to uruchamiam szybkiego crona
    '''try:
        ftest=open('test.test','rw')
        ftest.write('test')
    except:
        #system ro
        variableSave('now', '/tmp/zelVC_configNow.txt')#blokowanie szukania sieci jeżeli konfiguracja
        variableSave('now', '/tmp/zelVC_confInstall.run')#szybka konfiguracja
    else:
        ftest.close()'''


    #end

    #flogg.close()
firmName='Firma'        
parentList={firmName:''}
#keyDir="keyDir"
#session_seconds= leSe.sesTime
     
      
def main(logV,now,timestamp):
    loggLevel=lvConf.loggLevel
    #logV=(__version__, loggLevel, timestamp,logName)
    lovV=(logV[0],loggLevel,logV[2],logV[3])
    vl=logV
    #flogg=False
    keyName='noName'
    generateForm=True
    session_seconds= lvConf.sesTime
    #------------log tworzony zawsze od nowa
    """if loggLevel:
        try:
            flogg=open(loggName, 'wb')
        except:
            #print "testtest3"
            os.system("echo 'I can't open logg file ' > error.txt")
            sys.exit("I can't open logg file")
    vl=(flogg,loggLevel,timestamp,__version__)"""
    printLogg2(logV,0,"---Start----")
    #print "Content-Type: text/html\n"
    #odczytujemy ciastko, jeżeli to możliwe
    #ciastko może być z nieważnej sessji.
    cookie = http.cookies.SimpleCookie()
    string_cookie = os.environ.get('HTTP_COOKIE')
    printLogg2(logV,0, "string_cookie = %s" % string_cookie)
    sid=-1
    sessionKey=-1
    userN=False
    service=lvConf.mainService
    printLogg2(vl,8, "userN = %s" % userN)
           
  
#odczytujemy formularz, jeżeli to możliwe
    form = cgi.FieldStorage()
    printLogg2(vl,8,"form = %s" % str(form))
    if ("service" in form):
        service=(form["service"].value)
        printLogg2(vl,8,"service = %s" % service)
        #if (form.has_key("username")):
            
        #if string_cookie and not(form.has_key("username")):#dobra nazwa serwisu umożliwiała zalogowanie
        if string_cookie and not("username" in form):#dobra nazwa serwisu umożliwiała zalogowanie
            try:
                cookie.load(string_cookie)
            except:
                printLogg2(vl,1, "I can't load the cookie")
            else:
                printLogg2(vl,5,"Session id from anywhere?= %s" % str(sid))
                try:
                    #sid = cookie['sess'].value
                    sid = int(cookie[service+'lvSu'].value)
                    printLogg2(vl,5,"Session id from cookie= %s" % str(sid))
                except:
                    printLogg2(vl,5,"I can't load session id 11 sid = %s" % str(sid))
                    generateForm=True
        #if (form.has_key("session_key")):
        printLogg2(vl,5,"Session id 3 = %s" % str(sid))
        #print userN
        '''
    else:
        lvSuSes.generate_form(inf="Nieznana nazwa serwisu",title='letronikVISITORS', header='Logowanie',panel='letronik')
        #service='letronik'
        sid=-1
        userN=False
        generateForm=False
'''

    printLogg2(vl,5,"Session id 1= %s" % str(sid))
    if sid>0:
        
        sessionKey= sid #form["session_key"].value.strip()
        keyName = os.path.join(keyDir,"key_%s.txt" % (sessionKey))
        userN=lvSuSes.fetch_username(keyName)
        #try:
            #service=userN.split('@')[1]
        #except:
            #service='main'
        #sledz(logV, 9,"sessionKey = %s" % str(sessionKey))
        #sledz(logV, 9,"userN = %s" % userN)
    printLogg2(vl,5,"Session user 1= %s service= %s" % (userN, service))
    if userN: #jeżeli isnieje plik sessji
                
        try:
            sys.path.append(service)
            import lvCf #plik konfiguracji
            import leSe
        except:
            lvSuSes.generate_form("1. serwis: %s nie istnieje, zła nazwa" % service)
        else:
            loggLevel=lvCf.loggLevel
            printLogg2(vl,5,"loggLevel 5 = %s" % str(loggLevel))
            #region=lvCf.region
            #firmName=lvCf.firmName
            #firmCode=lvCf.firmCode
            #direction = lvCf.direction
            #session_seconds= lvConf.sesTime

            ts=lvSuSes.fileAge("%s/key_%s.txt" % (keyDir,sessionKey),'s')
            printLogg2(vl,8,"Session age = %d" % ts)
        
            #print "form has key session_key = ->%s<- <br>" % sessionKey
            if ("" in form or ts>session_seconds):
                #print "form has key logout <br>"
                lvSuSes.delete_session(keyName)
                if ts>session_seconds:
                    lvSuSes.generate_form("Przekroczyłeś czas bezczynności.<br> %dsek &gt; MAX=%d <br>Możesz zalogować się ponownie."% (ts ,session_seconds))
                else:
                    lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.",panel=service)
            else:#ważma sessja
                result = lvSuSes.testFirm(service,userN , '','')
                #lvSuSes.display_page(userN,result,ts,cookie,sessionKey)
                core(logV,form,lvCf,leSe,userN,result,ts,cookie,sessionKey,service,vl)
                lvSuSes.write_session(userN,keyName)


        


    elif ("action" in form and "username" in form \
    and "password" in form and "panel" in form):
        if (form["action"].value == "display"):
            userN=form["username"].value
            service=form["panel"].value
            printLogg2(vl,8,"Service Name %s" % service)


                
            
            try:
                sys.path.append(service)
                import lvCf #plik konfiguracji
                import leSe
            except:
                lvSuSes.generate_form("2. serwis: %s nie istnieje, zła nazwa" % service)
            else:
                loggLevel=lvCf.loggLevel
                #region=lvCf.region
                #firmName=lvCf.firmName
                firmCode=lvCf.firmCode
                #direction = lvCf.direction
                #session_seconds= leSe.sesTime
                headerName=">%s<" % service
                siteTitle=">%s<" % service
                """
                try:
                    headerName=leSe.headerName
                    siteTitle=leSe.siteTitle
                except:
                    pass"""

                
                printLogg2(vl,6,"4. userN from form = %s,firmCode= %s" % (userN,firmCode))
                 
                result = lvSuSes.testFirm(service,userN , form["password"].value)
                printLogg2(vl,6,"result from form = %s" % str(result))
                if result[:3] != 'err':
                    ts=0
                    sessionKey=lvSuSes.create_session(userN,keyDir)
                    #lvSuSes.display_page(userN,result,ts,cookie, sessionKey)
                    printLogg2(vl,6,"before core: userN from form = %s,firmCode= %s, vl=%s" % (userN,firmCode,str(vl)))
                    core(logV,form,lvCf,leSe, userN,result,ts,cookie,sessionKey,service,vl)
                    #core(form, userN,result,ts, sessionKey)
                else:
                    lvSuSes.generate_form("Podałeś złe dane")
        else:
            lvSuSes.generate_form("Podałeś złe dane",panel='error')                    
    elif ("action" in form and not ("username" in form \
    and "password" in form and "panel" in form)):
        lvSuSes.generate_form("Brakuje danych")

    else:
        printLogg2(logV,6,"generateForm = %s" % generateForm)
        if generateForm:
            userDe=''
            panelDe=service
            if "panelD" in form:
                panelDe=form["panelD"].value
            if "userD" in form:
                userDe=form["userD"].value                
            #sledz(logV, 9,"Content-Type: text/html\n\nsessionKey = %s" % str(sessionKey))
            #sledz(logV, 9,"userN = %s" % userN)
            if loggLevel>0:
                #przy nowym logowaniu loggLevel zawsze równy 0
                lvSuSes.generate_form("Witamy.<br>\
Nie znaleziono użytkownika sesji U=%s C=%s K=%s" % (userN,string_cookie,keyName),panelD=panelDe,userD=userDe)
            else:
              
                lvSuSes.generate_form("Witamy w serwisie letronikVISITORS.",panelD=panelDe,userD=userDe)
    if loggLevel:
        try:
            flogg.close()
        except:
            pass

script=os.path.basename(sys.argv[0])     
sys.stderr = sys.stdout

#now=datetime.now()
#timestamp=now.strftime("%Y%m%d %H:%M:%S ")

main(logV,now,timestamp)


    
