#! /usr/bin/env python3
# -*- coding: utf-8 -*-
__version__ = '202102171338'
'''
     Licencja ISC (ISCL)

     Prawo autorskie (c) 2010-2015. Leszek Loboda - Zakład Elektroniczny Letronik.

     Uprawnienie do używania, kopiowania, modyfikowania i / lub rozpowszechniania tego programu w dowolnym celu, 
     z lub bez opłat zostaje przyznany, pod warunkiem, że powyższe informacje o prawach autorskich i niniejsze 
     pozwolenie pojawią się na wszystkich kopiach. 
     OPROGRAMOWANIE JEST DOSTARCZANE "TAK JAK JEST" I AUTOR NIE UDZIELA ŻADNYCH GWARANCJI W ODNIESIENIU DO 
     TEGO OPROGRAMOWANIA, W TYM DOMYŚLNYCH GWARANCJI PRZYDATNOŚCI HANDLOWEJ I ZDATNOŚCI. 
     W ŻADNYM WYPADKU AUTOR NIE PONOSI ODPOWIEDZIALNOŚCI ZA ŻADNE SZCZEGÓLNE, BEZPOŚREDNIE, 
     POŚREDNIE LUB WTÓRNE LUB JAKIEKOLWIEK SZKODY, W TYM WYNIKAJĄCE Z UTRATY DANYCH LUB ZYSKÓW, 
     CZY TO W RAMACH UMOWY, ZANIEDBANIA LUB INNYCH CZYNÓW NIEDOZWOLONYCH , WYNIKAJĄCE Z / LUB W ZWIĄZKU 
     Z UŻYCIEM /LUB DZIAŁANIEM TEGO OPROGRAMOWANIA.

     ISC License (ISCL)

     Copyright (c) 2010-2020. Leszek Loboda - Zakład Elektroniczny Letronik.

     Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee 
     is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
     THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
     SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. 
     IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, 
     OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
     DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
     ARISING OUT OF OR IN CONNECTION WITH THE USE

'''


'''

lvUser.py start 20190227
202008181640
group=xbody.split(b':')[1].decode()
202006261316
python3
201810251520
    if form.getvalue('newGroup'):
        newGroup = (form.getvalue('newGroup'))
        printLogg2(logV,1, "newGroup=%s"% newGroup)
        #testResult+=testText(newGroup)
201809271657
variableSave('now', '/tmp/zelVC_confInstall.run')#szybka konfiguracja
secretsList=os.listdir(secretsDir)
201809141522
zapis do tmp dla RO
20180523
https://www.w3schools.com/w3css/default.asp
wywoływany przez lvSu.py
korzysta z lvSuSes.py

sid = cookie[service+'lvSu'].value

20180829
Kasowanie użytkownika ver 12
20180503
kasuje i dodaje liczniki
próbuję przekazać serwic w ukrytym formularzu
usuń grupę
wypisuje liczniki z grupami
20180403
------------------
20180329
Wymagane pakiety
sudo pip install openpyxl
__version__ = '20180224_1143'
dirPy64int=os.path.join('..','py64int')
if os.path.isdir(dirPy64int):
    sys.path.append(dirPy64int)

20180502_1350
wykaz nowych tablic liczników
20180308_1145
20180306_1834
dirwin=os.path.join('..','pylibwin')
dirlin=os.path.join('..','pyliblin')
if os.name=='nt':
'''

loggLevel=9
import sys
import os
import types
from datetime import datetime, timedelta
import hashlib
import lvConf
from lvCommonLib import greens, blues,sledz,printLogg2,regionParents2,pHelp,countersInRegion,dayTimes


def variableSave(variable, fileName):
    try:
        fn=open(fileName,'wb')
    except:
        return "cant open %s" % fileName
    fn.write(variable)
    fn.close
    return 0
def testText(text):
    if len(text)>25:
        return "%s - błąd do 10 znaków" % text
    if not text.isalnum():
        return "%s - błąd tylko licery i cyfry bez przerw" % text
    return ''
def testAsciiNum(text):
    if len(text)>25:
        return "%s - błąd do 25 znaków" % text
    try:
        text.encode('ascii')
    except UnicodeEncodeError:
        return "%s - błąd tylko licery łącińskie i cyfry bez przerw" % text
          
    
    if not text.isalnum():
        return "%s - błąd tylko licery łącińskie i cyfry bez przerw" % text
    return ''

def testUnicod(text):
    if len(text)>25:
        return "%s - błąd do 25 znaków" % text
    #text1=unidecode(text.decode('utf-8'))
    if not text.isalnum():
        return "%s - błąd tylko licery i cyfry bez przerw" % text
    
    return ''
def deleteSecret(path,login):
    fileName=os.path.join(path,login+'_secret.txt')
    secretTmp=os.path.join('/tmp',login+'_secret.act')
    try:
        os.remove(fileName)

    except:
        try:
            fp=open(secretTmp,'wb')
            fp.write('delete:%s\n' % fileName)
            fp.flush()
            fp.close

        except:
            return ' Błąd kasowania użytkownika!'
        else:
            return ' Rozkaz kasowania %s do /tmp' % login

    
    return ' Użytkownik skasowany %s ' % login





def makeSecretOld(path,grupa,login,haslo):
    secret= ' '+hashlib.md5(haslo).hexdigest()+':'+grupa
    fileName=os.path.join(path,login+'_secret.txt')
    try:
        fp=open(fileName,'wb')
        fp.write()
        fp.flush()
        fp.close
    except:
        secret=' Błąd zapisu pliku hasła!'
    return secret
def makeSecret(path,grupa,login,haslo):
    secret= ' '+hashlib.md5(haslo.encode()).hexdigest()+':'+grupa
    fileName=os.path.join(path,login+'_secret.txt')
    secretTmp=os.path.join('/tmp',login+'_secret.txt')
    secretCom=os.path.join('/tmp',login+'_secret.act')
    secretCommand='copy:%s:%s\n' % (secretTmp,fileName)
    try:
        fp=open(fileName,'wb')
        fp.write(secret.encode())
        fp.flush()
        fp.close
    except:
        try:#jeżeli sytem RO
            fp=open(secretTmp,'wb')
            fp.write(secret)
            fp.flush()
            fp.close
            fp=open(secretCom,'wb')
            fp.write(secretCommand)
            fp.flush()
            fp.close            
        except:
            secret=' Błąd zapisu pliku hasła!'        
        else:
            secret += ' /tmp '
    return secret
#sys.path.append("py")


workDir=os.path.dirname(sys.argv[0])
tmpDir=os.path.join(workDir,"tmp")
logName=os.path.join(tmpDir,'zelVC_lvUsers.log')
flogg=open(logName,'w')#kasowanie loggu
flogg.close()
now = datetime.now()
timestamp=now.strftime("%Y%m%d %H:%M:%S ")
loggLevel=lvConf.loggLevel
logV=(__version__, loggLevel, timestamp,logName)
printLogg2(logV)
printLogg2(logV,0,workDir)
sys.path.append(os.path.join('..','conf'))
dirwin=os.path.join('./','pylibwin')
dirlin=os.path.join('./','pyliblin')
if os.name=='nt':
    if os.path.isdir(dirwin):
        sys.path.append(dirwin)
        #print "lib %s" % dirwin

else:
    if os.name=='posix':
        if os.path.isdir(dirlin):
            sys.path.append(dirlin)
            #print "lib %s" % dirlin
    else:
        print("Nierozpoznany system %s" % os.name)
        sys.exit(-1)

printLogg2(logV,0,os.name)
dirPy64=os.path.join('..','py64')
if os.path.isdir(dirPy64):
    sys.path.append(dirPy64)
#import pickle
#import shutil
import string
#import filecmp
#import ftplib #import FTP
from unidecode import unidecode
#import socket

#from openpyxl import Workbook
import time
#import subprocess

#import MySQLdb as mdb
#from lscLib import printLogg2,insStr, dayList
import cgi
import cgitb; cgitb.enable()
import http.cookies
import lvSuSes





helpLevel=0
skrypt=os.path.basename(sys.argv[0])
scriptPath=os.path.dirname(sys.argv[0])
keyDir=os.path.join(scriptPath,'keyDir')
#print keyDir
firmName=''
firmCode=''
vl=()
chartNo=0
sys.stderr = sys.stdout
#print "Content-Type: text/html\n\n"

printLogg2(logV)

def userTest(title,session,user,userFirm,firmCode):
    '''sprawdza, czy użykownik ma uprawnienia.'''

    if userFirm!=firmCode:#userFirm pobrany z 'sekretu' firmCode z pliku lvCf.py
        print('''<!-- start(title,session,user='',firm='')-->\n''')
        print("<h1>%s</h1>" % title)
        print('''<p style="font-size: 12px;">Skrypt %s wersja %s</p>'''% (script,__version__))        
        print('''<h3>!!!Nie masz uprawnień do -%s- masz uprawnienia do -%s-!!!</h3>''' % (firmCode,userFirm))
        footer()
        sys.exit(0)

def footer():
    '''wstawia zamknięcie strony'''
    print('''<!-- footer()-->\n''')
    print("<h2> Program wykonał się do końca. </h2>")
    print("</div><!--body2 w lvMap.py--></body></html>")        




        

#mtg-----------------malowanie tablicy godzin stop
    
#def core(form,lvCf, userN,firmName,firmCode,direction,result,ts,cookie,sessionKey,service,vl):
def core(logV,form,lvCf,leSe, userN,result,ts,cookie,sessionKey,service,vl):
    
    
#START###########################         START START START START START START START START START START START START
    #firmDir=lvCf.firmDir
    
    firmName=lvCf.firmName
    global cur, counterLs, helpLevel, kolorDict,workDir
    global counterList,parentList
    global sld, wkl, tto
    #printLogg2(logV,1,"firmDir = %s firmName=%s" % (firmDir,firmName))
    try:
        headerName=leSe.headerName
        siteTitle=leSe.siteTitle
    except:
        headerName=service
        siteTitle=service+'_Users'

 
    
    #loggLevel=9
    #lvSuSes.display_sess(userN,cookie,sessionKey,firmName,"Przeglądarka odwiedzalności")
    lvSuSes.display_sess(userN,service,cookie,sessionKey,ts,result,siteTitle)
    userTest("letronikVISITORS",sessionKey,userN,result,'su')    
    #lvSuSes.data['logged_until'] = time.time() + 10*60 #po 60 sekundach sesja jest przerywana
    #lvSuSes.cookie
    #lvSuSes.data.setdefault('output', [])
    printLogg2(logV,1, "userTest OK")
    regionToCheck =''# do przekazania w polu ukrytym do sprawdzania chechBoxów

    #---------aktualizacja daty  
    now = datetime.now()
    #--------godziny do zapytania sql

    #fileNumber=0
    #lastFile='noFile'
    #----------data do logu
    timestamp=now.strftime("%Y%m%d %H:%M:%S ")


    newUser=''
    testResult=''
    warning=''
    if form.getvalue('newUser'):
        newUser = (form.getvalue('newUser'))
        printLogg2(logV,1, "newUser=%s"% newUser)
        s=testAsciiNum(newUser)
        testResult+=s
        warning+=s
    newGroup=''
    if form.getvalue('newGroup'):
        newGroup = (form.getvalue('newGroup'))
        printLogg2(logV,1, "newGroup=%s"% newGroup)
        s=testUnicod(newGroup)
        testResult+=s
        warning+=s
    newPass1=''
    if form.getvalue('newPass1'):
        newPass1 = (form.getvalue('newPass1'))
        printLogg2(logV,1, "newPass1=%s"% newGroup)
        s=testAsciiNum(newPass1)
        testResult+=s
        warning+=s
    newPass2=''
    if form.getvalue('newPass2'):
        newPass2 = (form.getvalue('newPass2'))
        printLogg2(logV,1, "newPass2=%s"% newGroup)
        testResult+=testText(newPass2) 
        if newPass1!=newPass2:
            newUserOk=False
            warning+="Hasła różne. "
        else:
            diff=False
    toDelete=False
    if form.getvalue('toDelete'):
        toDelete = (form.getvalue('toDelete'))
        printLogg2(logV,1, "toDelete=%s"% toDelete)
        warning+=(toDelete)  
            
    #warning=''
    if newUser and newGroup and newPass1 and newPass2:
        newUserOK=True

    else:
        newUserOK=False
        if newUser or newGroup or newPass1 or newPass2:
            warning+='Dane niekompletne. '
    if testResult!='':
        newUserOK=False
        #warning+="Tylko litery i cyfry do 10 znaków. "
    #warning+=str(newUserOK)
    region=(firmName,'g')
    parentList={firmName:''}
    serviceDir=os.path.join(workDir,service)
    secretsDir=os.path.join(serviceDir,"secrets")
    secretsList=os.listdir(secretsDir) 


    

    if toDelete:
        warning+=deleteSecret(secretsDir,toDelete)
        newUser=''
        newGroup=''
        newPass1=''
        newPass2=''        
    else:
        if newUserOK:
            warning+=newUser+makeSecret(secretsDir,newGroup,newUser,newPass2)
            newUser=''
            newGroup=''
            newPass1=''
            newPass2=''
        else:
            testResult=''
        
            for secret in secretsList:
                xf=open(os.path.join(secretsDir,secret), 'rb')
                xbody=xf.read()
                xf.close()
                user=secret.split('_')[0]
                try:
                    group=xbody.split(b':')[1].decode()
                except:
                    group = 'BŁĄD'

                newPass1=False
                if form.getvalue('pass1_%s' % user):
                    newPass1 = (form.getvalue('pass1_%s' % user))
                    printLogg2(logV,1, "pass1_%s=%s"% (user,newPass1))
                    s=testAsciiNum(newPass1)
                    testResult+=s
                    warning+=s                    
                newPass2=False
                if form.getvalue('pass2_%s' % user):
                    newPass2 = (form.getvalue('pass2_%s' % user))
                    printLogg2(logV,1, "pass2_%s=%s"% (user,newPass1))
                    s=testAsciiNum(newPass2)
                    testResult+=s
                    warning+=s
                if newPass2 and newPass1:
                    if newPass1!=newPass2:
                        warning+="Hasła %s różne. " % user
                    else:
                        if testResult=='':
                            warning+=' new passw ok for ' +user+makeSecret(secretsDir,group,user,newPass2)
                        else:
                            warning+="Tylko litery i cyfry do 10 znaków. "
                else:
                    if newPass2 or newPass1:
                        warning+="Brakuje danych. "
                    

        


        
    #----wczytywanie firmName   
    #if form.getvalue('firmSym'):
       #firmName = form.getvalue('firmSym')
    #region=(firmName,'g')
    #parentList={firmName:''}
    #serviceDir=os.path.join(workDir,service)
    #secretsDir=os.path.join(serviceDir,"secrets")
    #secretsList=os.listdir(secretsDir)

          
    print("<p><font style=\"font-size: 10px;\">letronik </font>")
    print("<font style=\"font-size: 20px;\">VISITORSconfig</font>")
    print("<font style=\"font-size: 14px;\"><sup>%s</sup></font>" % __version__)
    print("<font style=\"font-size: 10px;\">&nbsp;&nbsp;dla firmy:</font>") 
    print("<font style=\"font-size: 15px;\">%s</font>"  % headerName)
    #print """<h1>Konfigurator użytkowników serwisu "%s"</h1>""" % service
    print("""<h2>Konfiguracja servisu %s <span style="font-size: 12px"><a href="%s?service=%s">Odśwież</a>\
<br>Możesz zmienić hasło, dodać lub usunąć użytkonwika.""" % (service,script,service))
    try:
        ftest=open('test.test','w')
        ftest.write('test')
    except:
        #system ro
        print(" System RO</span></h2>")
        variableSave('now', '/tmp/zelVC_configNow.txt')#blokowanie szukania sieci jeżeli konfiguracja
        variableSave('now', '/tmp/zelVC_confInstall.run')#szybka konfiguracja
    else:
        ftest.close()
        print(" System RW</span></h2>")
    print(""" <div class="w3-container">""")
    print("""<form method = "post" action= "%s?service=%s">""" % (script,service))
    print(""" <input type="hidden" name="sevice" value="%s"> """ % service)
    print("""<table class="w3-table-all"><tr>
<th >Usuń</th><th>Użytkownik</th><th >Grupa</th><th>Hasło</th><th>Powtórka h.</th></tr>""")
    secretsList=os.listdir(secretsDir)
    for secret in secretsList:
        if secret.find('.txt')>-1:
            xf=open(os.path.join(secretsDir,secret), 'rb')
            xbody=xf.read()
            xf.close()

            user=secret.split('_')[0]
            try:
                group=xbody.split(b':')[1].decode()
            except:
                group = "błąd"
            print("""<tr>
<td><input class="w3-radio" type="radio" name="toDelete" value="%s"></td>
<td >%s</td>
<td>%s</td><td><input type="password" name="pass1_%s" maxlength="12" size="16"></td>
<td><input type="password" name="pass2_%s" maxlength="12" size="16"></td>

</tr>""" % (user,user,str(group),user,user))
    print("""<tr>
<td>Nowy</td>
<td><input type="text" name="newUser" value="%s" maxlength="12" size="16"></td>
<td><input type="text" name="newGroup" value= "%s" maxlength="25" size="16"></td>
<td><input type="password" name="newPass1"  maxlength="12" size="16"></td>
<td><input type="password" name="newPass2"  maxlength="12" size="16"></td>

</td></tr></table>

""" % (newUser,newGroup))
    if len(warning)<3:
        print("""<br><INPUT TYPE = "submit" VALUE = "Zatwierdź">&nbsp;&nbsp;""")
        
    print("""<a href="%s?service=%s">Odśwież</a> &nbsp; &nbsp;<b>%s</b><br>""" %(script,service,warning))
    print("""<a href="lvSu.py?service=%s">Powrót</a></form></div>  """ % service)
   
                
        
  
    



    #end

    #flogg.close()
firmName='Firma'        
parentList={firmName:''}
#keyDir="keyDir"
#session_seconds= leSe.sesTime
     
      
def main(logV,now,timestamp):
    vl=logV
    loggLevel=lvConf.loggLevel
    flogg=False
    keyName='noName'
    generateForm=True
    session_seconds= lvConf.sesTime
    #------------log tworzony zawsze od nowa
    if loggLevel:
        try:
            flogg=open(loggName, 'wb')
        except:
            #print "testtest3"
            os.system("echo 'I can't open logg file ' > error.txt")
            sys.exit("I can't open logg file")
    #vl=(flogg,loggLevel,timestamp,__version__)
    printLogg2(vl,0,"---Start----")
    #print "Content-Type: text/html\n"
    #odczytujemy ciastko, jeżeli to możliwe
    #ciastko może być z nieważnej sessji.
    cookie = http.cookies.SimpleCookie()
    string_cookie = os.environ.get('HTTP_COOKIE')
    printLogg2(vl,0, "string_cookie = %s" % string_cookie)
    sid=-1
    sessionKey=-1
    userN=False
    service=lvConf.mainService
    printLogg2(vl,0, "userN = %s" % userN)
           
  
#odczytujemy formularz, jeżeli to możliwe
    form = cgi.FieldStorage()
    printLogg2(vl,0,"form = %s" % str(form))
    if ("service" in form):
        service=(form["service"].value)
        printLogg2(vl,8,"service = %s" % service)
        #if (form.has_key("username")):
            
        #if string_cookie and not(form.has_key("username")):#dobra nazwa serwisu umożliwiała zalogowanie
        if string_cookie and not("username" in form):#dobra nazwa serwisu umożliwiała zalogowanie
            try:
                cookie.load(string_cookie)
            except:
                printLogg2(vl,1, "I can't load the cookie")
            else:
                printLogg2(vl,5,"Session id from anywhere?= %s" % str(sid))
                try:
                    #sid = cookie['sess'].value
                    sid = int(cookie[service+'lvSu'].value)
                    printLogg2(vl,5,"Session id from cookie= %s" % str(sid))
                except:
                    printLogg2(vl,5,"I can't load session id 11 sid = %s" % str(sid))
                    generateForm=True
        #if (form.has_key("session_key")):
        printLogg2(vl,5,"Session id 3 = %s" % str(sid))
        #print userN
        '''
    else:
        lvSuSes.generate_form(inf="Nieznana nazwa serwisu",title='letronikVISITORS', header='Logowanie',panel='letronik')
        #service='letronik'
        sid=-1
        userN=False
        generateForm=False
'''

    printLogg2(vl,5,"Session id 1= %s" % str(sid))
    if sid>0:
        
        sessionKey= sid #form["session_key"].value.strip()
        keyName = os.path.join(keyDir,"key_%s.txt" % (sessionKey))
        userN=lvSuSes.fetch_username(keyName)
        #try:
            #service=userN.split('@')[1]
        #except:
            #service='main'
        #sledz(logV, 9,"sessionKey = %s" % str(sessionKey))
        #sledz(logV, 9,"userN = %s" % userN)
    printLogg2(vl,5,"Session user 1= %s" % userN)
    if userN: #jeżeli isnieje plik sessji
                
        try:
            sys.path.append(service)
            import lvCf #plik konfiguracji
            import leSe
        except:
            lvSuSes.generate_form("serwis nie istnieje, zła nazwa")
        else:
            loggLevel=lvCf.loggLevel
            #region=lvCf.region
            #firmName=lvCf.firmName
            #firmCode=lvCf.firmCode
            #direction = lvCf.direction
            #session_seconds= lvConf.sesTime

            ts=lvSuSes.fileAge("%s/key_%s.txt" % (keyDir,sessionKey),'s')
            printLogg2(vl,8,"Session age = %d" % ts)
        
            #print "form has key session_key = ->%s<- <br>" % sessionKey
            if ("" in form or ts>session_seconds):
                #print "form has key logout <br>"
                lvSuSes.delete_session(keyName)
                if ts>session_seconds:
                    lvSuSes.generate_form("Przekroczyłeś czas bezczynności.<br> %dsek &gt; MAX=%d <br>Możesz zalogować się ponownie."% (ts ,session_seconds))
                else:
                    lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.",panel=service)
            else:#ważma sessja
                result = lvSuSes.testFirm(service,userN , '','')
                #lvSuSes.display_page(userN,result,ts,cookie,sessionKey)
                core(logV,form,lvCf,leSe,userN,result,ts,cookie,sessionKey,service,vl)
                lvSuSes.write_session(userN,keyName)


        


    elif ("action" in form and "username" in form \
    and "password" in form and "panel" in form):
        if (form["action"].value == "display"):
            userN=form["username"].value
            service=form["panel"].value
            printLogg2(vl,8,"Service Name %s" % service)


                
            
            try:
                sys.path.append(service)
                import lvCf #plik konfiguracji
                import leSe
            except:
                lvSuSes.generate_form("serwis nie istnieje, zła nazwa")
            else:
                loggLevel=lvCf.loggLevel
                #region=lvCf.region
                #firmName=lvCf.firmName
                firmCode=lvCf.firmCode
                #direction = lvCf.direction
                #session_seconds= leSe.sesTime
                headerName=">%s<" % service
                siteTitle=">%s<" % service
                """
                try:
                    headerName=leSe.headerName
                    siteTitle=leSe.siteTitle
                except:
                    pass"""

                
                printLogg2(vl,6,"4. userN from form = %s,firmCode= %s" % (userN,firmCode))
                 
                result = lvSuSes.testFirm(service,userN , form["password"].value)
                printLogg2(vl,6,"result from form = %s" % str(result))
                if result[:3] != 'err':
                    ts=0
                    sessionKey=lvSuSes.create_session(userN,keyDir)
                    #lvSuSes.display_page(userN,result,ts,cookie, sessionKey)
                    printLogg2(vl,6,"before core: userN from form = %s,firmCode= %s, vl=%s" % (userN,firmCode,str(vl)))
                    core(logV,form,lvCf,leSe, userN,result,ts,cookie,sessionKey,service,vl)
                    #core(form, userN,result,ts, sessionKey)
                else:
                    lvSuSes.generate_form("Podałeś złe dane")
        else:
            lvSuSes.generate_form("Podałeś złe dane",panel='error')                    
    elif ("action" in form and not ("username" in form \
    and "password" in form and "panel" in form)):
        lvSuSes.generate_form("Brakuje danych")

    else:
        if generateForm:
            userDe=''
            panelDe=service
            if "panelD" in form:
                panelDe=form["panelD"].value
            if "userD" in form:
                userDe=form["userD"].value                
            sledz(logV, 9,"Content-Type: text/html\n\nsessionKey = %s" % str(sessionKey))
            sledz(logV, 9,"userN = %s" % userN)
            if loggLevel>0:
                #przy nowym logowaniu loggLevel zawsze równy 0
                lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.<br>\
Nie znaleziono użytkownika sesji U=%s C=%s K=%s" % (userN,string_cookie,keyName),panelD=panelDe,userD=userDe)
            else:
              
                lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.",panelD=panelDe,userD=userDe)
    if loggLevel:
        try:
            flogg.close()
        except:
            pass

script=os.path.basename(sys.argv[0])     
sys.stderr = sys.stdout

#now=datetime.now()
#timestamp=now.strftime("%Y%m%d %H:%M:%S ")

main(logV,now,timestamp)


    
