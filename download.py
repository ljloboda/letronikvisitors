#!/usr/bin/python3
#-*- coding: utf-8 -*-
#20180321
#    if lvConf.upDirTmp:
#         path='tmp'
# skrypt pozwala pobierać plik 
#print '<p><a href="download.py?path=demo2&filename=days.csv">Pobierz paczkę z konfiguracją</p>'

import sys
import os
import cgi
import cgitb; cgitb.enable()
import lvConf
from shutil import copyfileobj

# HTTP Header

form = cgi.FieldStorage()

if ("filename" in form):
    filename=(form["filename"].value)
    if lvConf.upDirTmp:
        path='tmp'
    else:
        if ("path" in form):
            path=(form["path"].value)
        else:
            print("path error")
            sys.exit("path error")
    print("Content-type: application/octet-stream")
    print("Content-Disposition: attachment; filename=%s" % filename)
    print()
    sys.stdout.flush()
    filepath=(os.path.join(path, filename))
    with open(filepath,'rb') as zipfile:
        copyfileobj(zipfile, sys.stdout.buffer)


