#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import cgitb
cgitb.enable()

print ("Content-Type: text/html\n\n")
print ('<html>')
print ('<head>')
print ('<title>Hello Word - First CGI Program</title>')
print ('</head>')
print ('<body>')
print('<h2>Hello Word! This is my (Leszka) first CGI program</h2>')
print('<pre>')
for n in range(0,3):
    for m in range(1,10):
        for k in range(1,4):
            print(m,'*',n*3+k,'=',m*(n*3+k),'\t', end=' ')
        print() 
    print()
print('</pre>')
print('</body>')
print('</html>')





