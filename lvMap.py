#! /usr/bin/env python3
# -*- coding: utf-8 -*-

__version__ = '202102211837'

'''
     Licencja ISC (ISCL)

     Prawo autorskie (c) 2010-2015. Leszek Loboda - Zakład Elektroniczny Letronik.

     Uprawnienie do używania, kopiowania, modyfikowania i / lub rozpowszechniania tego programu w dowolnym celu, 
     z lub bez opłat zostaje przyznany, pod warunkiem, że powyższe informacje o prawach autorskich i niniejsze 
     pozwolenie pojawią się na wszystkich kopiach. 
     OPROGRAMOWANIE JEST DOSTARCZANE "TAK JAK JEST" I AUTOR NIE UDZIELA ŻADNYCH GWARANCJI W ODNIESIENIU DO 
     TEGO OPROGRAMOWANIA, W TYM DOMYŚLNYCH GWARANCJI PRZYDATNOŚCI HANDLOWEJ I ZDATNOŚCI. 
     W ŻADNYM WYPADKU AUTOR NIE PONOSI ODPOWIEDZIALNOŚCI ZA ŻADNE SZCZEGÓLNE, BEZPOŚREDNIE, 
     POŚREDNIE LUB WTÓRNE LUB JAKIEKOLWIEK SZKODY, W TYM WYNIKAJĄCE Z UTRATY DANYCH LUB ZYSKÓW, 
     CZY TO W RAMACH UMOWY, ZANIEDBANIA LUB INNYCH CZYNÓW NIEDOZWOLONYCH , WYNIKAJĄCE Z / LUB W ZWIĄZKU 
     Z UŻYCIEM /LUB DZIAŁANIEM TEGO OPROGRAMOWANIA.

     ISC License (ISCL)

     Copyright (c) 2010-2018. Leszek Loboda - Zakład Elektroniczny Letronik.

     Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee 
     is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
     THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
     SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. 
     IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, 
     OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, 
     DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
     ARISING OUT OF OR IN CONNECTION WITH THE USE

'''


'''
lvMap.py start 20180329
wywoływany przez lvSu.py
zysta z lvSuSes.py
202006260747
sid = int(cookie[service+'lvSu'].value)
#dirwin=os.path.join('..','pylibwin')
#dirlin=os.path.join('..','pyliblin')
dirwin=os.path.join('./','pylibwin')
dirlin=os.path.join('./','pyliblin')
python3
201911131712
nowa
def allTables():
sid = cookie[service+'lvSu'].value
20181121
 primary key (parent,child,sign)) character set utf8mb4 collate utf8mb4_unicode_ci;
20180627
character set utf8 collate utf8_polish_ci;
20180614
#sprawdzić czy jest tabela i jak nie, to utworzyć.
    cur.execute("CREATE TABLE IF NOT EXISTS regions (parent char(40)  not null , \
                     child char(40), \
                     sign char(4),\
                     primary key (parent,child,sign));" )

20180503
kasuje i dodaje liczniki
próbuję przekazać serwic w ukrytym formularzu
usuń grupę
wypisuje liczniki z grupami
20180403
------------------
20180329
Wymagane pakiety
sudo pip install openpyxl
__version__ = '20180224_1143'
dirPy64int=os.path.join('..','py64int')
if os.path.isdir(dirPy64int):
    sys.path.append(dirPy64int)

20180502_1350
wykaz nowych tablic liczników
20180308_1145
20180306_1834
dirwin=os.path.join('..','pylibwin')
dirlin=os.path.join('..','pyliblin')
if os.name=='nt':
'''

loggLevel=0
import sys
import os
import types
from datetime import datetime, timedelta
import lvConf
from lvCommonLib import greens, blues,sledz,printLogg2,regionParents2,pHelp,countersInRegion

#sys.path.append("py")
workDir=os.path.dirname(sys.argv[0])
tmpDir=os.path.join(workDir,"tmp")
logName=os.path.join(tmpDir,'zelVC_lvMap0.log')
flogg=open(logName,'w')#kasowanie loggu
flogg.close()
now = datetime.now()
timestamp=now.strftime("%Y%m%d %H:%M:%S ")
loggLevel=lvConf.loggLevel
logV=(__version__, loggLevel, timestamp,logName)
printLogg2(logV,0,"lovV=%s" % str(logV) )
printLogg2(logV,0,workDir)
sys.path.append(os.path.join('..','conf'))
#dirwin=os.path.join('..','pylibwin')
#dirlin=os.path.join('..','pyliblin')
dirwin=os.path.join('./','pylibwin')
dirlin=os.path.join('./','pyliblin')
if os.name=='nt':
    if os.path.isdir(dirwin):
        sys.path.append(dirwin)
        #print "lib %s" % dirwin

else:
    if os.name=='posix':
        if os.path.isdir(dirlin):
            sys.path.append(dirlin)
            #print "lib %s" % dirlin
    else:
        print("Nierozpoznany system %s" % os.name)
        sys.exit(-1)

printLogg2(logV,0,os.name)
dirPy64=os.path.join('..','py64')
if os.path.isdir(dirPy64):
    sys.path.append(dirPy64)
#import pickle
#import shutil
import string
#import filecmp
#import ftplib #import FTP
from unidecode import unidecode
#import socket

#from openpyxl import Workbook
import time
#import subprocess
try:
    import mysql.connector as mdb
except:
    print("""Content-Type: text/html


<html lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" >
<title>Awaria letronikVISITORS</title>
</head><body>
<h1><font color="red">Error:No module named mysql.connector / Błąd: brak modułu mysql.connector</font></h1></body>""")
    sys.exit('Błąd: brak modułu Mmysql.connector')
#from lscLib import printLogg2,insStr, dayList
import cgi
import cgitb; cgitb.enable()
import http.cookies
import lvSuSes

#from lvMonthLib import  monthsDrawCB,monthData,monthsMake





#

#__version__ = 'A141001-02'
#def sledz(loggLevel, level,tresc):
#def pHelp(level,tresc):
#def dayTimes(st,ft):
#def login(form, sess):
#def cycleH(day,openH,closeH):
#def byMonths2(sy,sm,sdd, shh, shm,fy,fm,fdd, fhh, fhm):
#def byMonths(sd,fd):
#def counterTab(configNameV):
#def regionCounters(region,level,xl=0):
#def countersInRegion(region,cur):
#def regionChildren(region):
#def regionParents():
#def regionParents1():
#
#mtcbF -----------Malowanie tablicy check-box funkcja  197
#mtdcb malowanie tablicy dni
#mtg-----------------malowanie tablicy godzin start
#START###########################         START START START START START START START START START START START START
#wdzf------------wczytanie danych z formularza
#wcko--------------wyciąganie czasów końca okresu
#wcpo--------------wyciąganie czasów początku okresu
#ccbr wczytywanie chechboxow regionow  449
#oswr odczytywanie starego wyboru regionów
#ort------ odczytywanie regionTree
#wrn wczytywanie radio nawigacji 475
#wddscb wyznaczanie nazw dni do sprawdzania checkboxów  -sld
sld=1
wkl=1
tto=1
#dayListM wykaz miesięcy dni do analizy 541
#wtmda ------------------ wybieranie tablic miesięcznych do analizy
#wligda------------------WYBIERANIE LICZNIKÓW I GRUP DO ANALIZY, obliczanie tablic obiektów 1017
#regRow=[(parentList[nazwa[0]],nazwa[0]),nazwa[1],0,0,0,0,False]
#kolorDict={} - słownik kolorów argument - nazwa rebiektu

#ddwd ---------   dane do wykresu dziennego start 658
#ddtg ----------  dane do tablicy godzinowej
#wynaw wyświetlanie nawigacji 882




#import lvCf #plik konfiguracji
#import leSe

#sys.path.append(os.path.dirname(os.environ['SCRIPT_FILENAME']))
#konfiguracja

'''hostDB="localhost"
userDB="12398781_dc"
pwdDB=""
dbDB="12398781_dc"
portDB=3306'''

helpLevel=0
skrypt=os.path.basename(sys.argv[0])
scriptPath=os.path.dirname(sys.argv[0])
keyDir=os.path.join(scriptPath,'keyDir')
#print keyDir
firmName=''
firmCode=''
vl=()
chartNo=0
sys.stderr = sys.stdout
#print "Content-Type: text/html\n\n"
#counterList=[]

printLogg2(logV)

def userTest(title,session,user,userFirm,firmCode):
    '''sprawdza, czy użykownik ma uprawnienia.'''

    if userFirm!=firmCode:#userFirm pobrany z 'sekretu' firmCode z pliku lvCf.py
        print('''<!-- start(title,session,user='',firm='')-->\n''')
        print("<h1>%s</h1>" % title)
        print('''<p style="font-size: 12px;">Skrypt %s wersja %s</p>'''% (script,__version__))        
        print('''<h3>!!!Nie masz uprawnień do -%s- masz uprawnienia do -%s-!!!</h3>''' % (firmCode,userFirm))
        footer()
        sys.exit(0)

def footer():
    '''wstawia zamknięcie strony'''
    print('''<!-- footer()-->\n''')
    print("<h2> Program wykonał się do końca. </h2>")
    print("</div><!--body2 w lvMap.py--></body></html>")        




               
region=''
counterList={}
counterList[region]=[]
mapa=''
regions=region
printLogg2(logV,2,"region = %s" % str(region))

outStr=''



def regionCountersOld(region,level,xl=0):
    global cur,now, countList,mapa,regions, outStr
   
    #print region, xl
    if region[1]=='g':
        if xl==0:
            child=region[0]
            sign=region[1]
            mapa="""<span style="text-decoration:underline;">%s</span> <span style="width: 600px; background-color: green;   margin: 10px auto;
  padding: 0px;
  border-style: solid;
  border-color: black;
  border-width: 1px;"><input name=\"addParent\" value=\"%s\" type=\"radio\">+</span>\n""" % (child,child) 
        if xl==level:
            regions=region
            counterList[regions]=[]
        xl+=1
        tab=''
        for t in range(xl):
            tab+='      '
        command ="select child, sign from regions where parent='%s' order by child;" % region[0]
        outStr+= "\n<br> %s" % command
        cur.execute(command)
        lista=cur.fetchall()
        outStr+= "\n<br> %s %d %d" % (str(lista),level,xl)
        for x in lista:
            child=x[0]
            sign=x[1]
            xmap=tab
            if notEmpty(child,cur):
                kminus=''
            else:
                kminus= """<span style="width: 600px; background-color: red;   margin: 10px auto;
  padding: 0px;
  border-style: solid;
  border-color: black;
  border-width: 1px;">-<input name=\"cancel\" value=\"%s\" type=\"radio\"></span>""" % child            
            if sign=='g':
                xmap+= """<span style="text-decoration:underline;">%s</span> <span style="width: 600px; background-color: green;   margin: 10px auto;
  padding: 0px;
  border-style: solid;
  border-color: black;
  border-width: 1px;">%s<input name=\"addParent\" value=\"%s\" type=\"radio\">+</span>""" % (child,kminus,child)
            else:
                xmap+= "%s %s %s" % (child,sign,kminus)

                
            #print xmap
            mapa+=xmap+'\n'
            if sign!='g':
                #print child,sign
                #countList.append((child,sign,level,xl))
                if xl>1:
                    counterList[regions].append((child,sign,region[0]))
                else:
                    counterList[(child,sign)]=[(child,sign,region[0])]
            else:
                
                regionCountersOld((child,sign),level,xl)
    else:
        command ="select parent from regions where child='%s' ;" % region[0]
        cur.execute(command)
        parentV=cur.fetchone()[0]
        #print "<br>Parent=%s, Region= %s" % (parentV,region[0])
        counterList[(region[0],region[1])]=[(region[0],region[1],region[0])]




def allTables():
    #wypisuje wszystkie tablice miesięczne
    global cur
    counters=[]
    #print "<h1>Wykaz tabel liczników</h1>"
    lista=[]#tylko miesięczne
    newTables=[]

    command ="SHOW TABLES;"
    cur.execute(command)
    lista1=cur.fetchall()
    #print "<br> lista1",lista1
    for x in lista1:
        if x[0].find('_')>-1:
            lista.append(x[0])
    #print "<br> lista",lista
    for x in lista:
        #print x,'<br>'
        x0=x.split('_')[0]
        if not x0 in counters:
            counters.append(x0)
    #print "<br> counters",counters
    #print "<h1>Wykaz liczników</h1>"
    for x in counters:
        #print x,'<br>'
        counterTables=[]
        for y in lista:
            #if y.find(x)>-1:
            if y.split('_')[0]==x:#20191113

                counterTables.append(y)
                #print y,"<br>"
        #print 'max',max(counterTables),'<br>'
        newTables.append(max(counterTables))
    return newTables
            
            
#countersInRegion start
#counterLs - wykaz liczników w regione jest konieczne do rekurencji, musi zostać.


#countersInRegion stop



#firmName='Firma'        
#parentList={firmName:''}

def parent(child,cur):
    """Podaje rodzica dla obiektu """
    command ="select parent from regions where child='%s' ;" % child
    cur.execute(command)
    parent=cur.fetchone()
    if parent:   
        parent=parent[0]
    else:
        parnet=False
    return parent

def removeRow(child,cur):
    """usuwanie wiersza"""
    command ="delete from regions where child='%s' ;" % child
    try:
        cur.execute(command)
    except:
        return False
    else:
        return True




def notEmpty(object,cur):
    """Podaje rodzica dla obiektu """
    command ="select child from regions where parent='%s' ;" % object
    cur.execute(command)
    child=cur.fetchone()
    if child:   
        return True
    else:
        return False
def regionParents():
    global counterList,parentList
    xlist=list(counterList.keys())
    for x in xlist:
        command ="select parent from regions where child='%s' ;" % x[0]
        cur.execute(command)
        parentV=cur.fetchone()
        if parentV:   
            parentList[x[0]]=parentV[0]
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''

    return xlist
        
def regionParents1():
    global regionLs,parentList, cur
    for x in regionLs:
        command ="select parent from regions where child='%s' ;" % x[0]
        cur.execute(command)
        parentV=cur.fetchone()
        if parentV:   
            parentList[x[0]]=parentV[0]
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''

    #return xlist

def regionParents2trash(lista,parentList, cur):
    #global parentList, cur
    for x in lista:
        printLogg2(logV,1, "21. regionParents2(lista)")
        command ="select parent from regions where child='%s' and sign='%s';" % (x[0],x[1])
        printLogg2(logV,1, command)
        cur.execute(command)
        parentV=cur.fetchone()
        printLogg2(logV,1, "22. parentV = %s" % str(parentV))
        #print parentV
        #sys.exit(1)
        if parentV:   
            parentList[x[0]]=parentV[0]
            printLogg2(logV,1,parentList[x[0]])
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''




        

#mtg-----------------malowanie tablicy godzin stop
    
#def core(form,lvCf, userN,firmName,firmCode,direction,result,ts,cookie,sessionKey,service,vl):
def core(logV,form,lvCf,leSe, userN,result,ts,cookie,sessionKey,service,vl):
    
    
#START###########################         START START START START START START START START START START START START
    #firmDir=lvCf.firmDir
    
    firmName=lvCf.firmName
    global cur, counterLs, helpLevel, kolorDict
    global counterList,parentList
    global sld, wkl, tto
    #printLogg2(logV,1,"firmDir = %s firmName=%s" % (firmDir,firmName))
    try:
        headerName=leSe.headerName
        siteTitle=leSe.siteTitle
    except:
        headerName=service
        siteTitle=service+'_Map'
    try:
        con = mdb.connect(host=lvCf.hostDB, user=lvCf.userDB, passwd=lvCf.pwdDB, db=lvCf.dbDB, port=int(lvCf.portDB))      
        cur = con.cursor()
    except:
        print("Content-Type: text/html\n")

        print('''<!DOCTYPE html>
<HTML lang="pl">
<HEAD><meta charset="utf-8"></HEAD><h1 style="text-align: center;">Nie można zalogować się do bazy - 1</h1>
    <div style="text-align: center;">Prawdopodobnie złe hasło.<br>
    <a href="./lv.py">Wróć do logowania.</a><br>
    </div>
    </body>
    </html>
    ''',lvCf.hostDB, lvCf.userDB,"tajne",lvCf.dbDB, lvCf.portDB)
        
        printLogg2(logV,1, "Nie udało się zalogować do bazy %s %s %s %s %s" % \
                   (lvCf.hostDB, lvCf.userDB,lvCf.pwdDB,lvCf.dbDB, lvCf.portDB))
        sys.exit("I can't open dbBase")
    sledz(logV, 5, """logowanie do bazy udane """)
    printLogg2(logV,1, """logowanie do bazy udane """)
 
    
    #loggLevel=9
    #lvSuSes.display_sess(userN,cookie,sessionKey,firmName,"Przeglądarka odwiedzalności")
    lvSuSes.display_sess(userN,service,cookie,sessionKey,ts,result,siteTitle)
    userTest("letronikVISITORS",sessionKey,userN,result,'su')    
    #lvSuSes.data['logged_until'] = time.time() + 10*60 #po 60 sekundach sesja jest przerywana
    #lvSuSes.cookie
    #lvSuSes.data.setdefault('output', [])
    printLogg2(logV,1, "userTest OK")
    regionToCheck =''# do przekazania w polu ukrytym do sprawdzania chechBoxów

    #---------aktualizacja daty  
    now = datetime.now()
    #--------godziny do zapytania sql

    #fileNumber=0
    #lastFile='noFile'
    #----------data do logu
    timestamp=now.strftime("%Y%m%d %H:%M:%S ")

    #dayName=now.strftime("%Y-%m-%d")

    #SfirmTrashCsvDir="../letronik/csvTrash"

    #fname=os.path.join(leSe.dirPrefix,lvCf.fname)


    #wdzf------------wczytanie danych z formularza
    #form = cgi.FieldStorage()
    if form.getvalue('debLev'):
        #print form.getvalue('debLev')
        lL = int(form.getvalue('debLev'))
        logV=(logV[0],lL,logV[2],logV[3])
        printLogg2(logV,1, "debLev=%d"% logV[1])
    if form.getvalue('helpLev'):
        helpLevel = int(form.getvalue('helpLev'))
        printLogg2(logV,1, "heplLev=%d"% helpLevel)
   
      
       
    #----wczytywanie firmName   
    if form.getvalue('firmSym'):
       firmName = form.getvalue('firmSym')
    region=(firmName,'g')
    parentList={firmName:''}
    #sprawdzić czy jest tabela i jak nie, to utworzyć.
    try:
        cur.execute("SHOW TABLES LIKE 'regions';")
    except:
        print("nie ma regions")
    else:
        lista=cur.fetchall()
        #print "tablica %d" % len(lista)
        if len(lista)==0:
            try:
                cur.execute("CREATE TABLE regions (parent char(40)  not null , \
                             child char(40), \
                             sign char(4),\
                             primary key (parent,child,sign)) character set utf8mb4 collate utf8mb4_unicode_ci;" )
            except:
                print("błąd tworzenia tablicy regions")
                cur.close()
                con.commit()
                con.close()
                sys.exit("błąd tworzenia tablicy regions")
          
    print("<p><font style=\"font-size: 10px;\">letronik </font>")
    print("<font style=\"font-size: 20px;\">VISITORSconfig</font>")
    print("<font style=\"font-size: 14px;\"><sup>%s</sup></font>" % __version__)
    print("<font style=\"font-size: 10px;\">&nbsp;&nbsp;dla firmy:</font>") 
    print("<font style=\"font-size: 15px;\">%s</font>"  % headerName)
    print("""<h1>Konfigurator mapy obiektów</h1>""")
    print("""<h2>Mapa sieci serwisu %s <span style="font-size: 12px"><a href="lvMap.py?service=%s">odśwież</a></span></h2>""" % (service,service))
    #print """Zaznacz czerwone pole aby usunąć, zielone pole i boxy przy obiektach, aby dodać.<br>Usuwanie ma priorytet nad dodawaniem<br><br>"""
    if form.getvalue('cancel'):
        cancel = (form.getvalue('cancel'))
        s=removeRow(cancel,cur)
        if s:
            print("""<div style="color: blue;">Usunięto %s </div>""" % cancel)
        else:
            print(" BŁĄD usuwania %s <br>" % cancel)
    else:
        if form.getvalue('addParent') and (form.getvalue('addChild') or form.getvalue('newGroup')):
            if form.getvalue('addParent') and form.getvalue('addChild'):
            
                addParent = (form.getvalue('addParent'))
                print("""<div style="color: green;">Dodawanie do grupy %s:<br> """ % addParent)
                addChilds = (form.getvalue('addChild'))
                if not isinstance(addChilds,list):
                    addChilds=[addChilds,]
                #print """&nbsp;&nbsp; obiektów %s <br>""" % str(addChilds)
                for ch in addChilds:
                    childL=ch.split('+')
                    child=childL[0]
                    sign=childL[1]

                    command="insert into regions values('%s','%s','%s');" %(addParent,child,sign)
                    #print command,'<br>'
                    try:
                        cur.execute(command)
                    except:
                        print("""&nbsp;&nbsp;&nbsp;<div style="color: red;">BŁĄD nie dodano %s,%s,%s<div>""" %(addParent,child,sign))
                    else:
                        print("""&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dodano %s,%s,%s<br>""" %(addParent,child,sign))
                print("</div>")
            if form.getvalue('addParent') and form.getvalue('newGroup'):
                addParent = (form.getvalue('addParent'))
                newGroup=form.getvalue("newGroup")
                #newGroupAsk=unidecode(newGroup.decode('utf-8'))
                #newGroupAsk = newGroup.encode()
                #if newGroupAsk.isalnum():
                if newGroup.isalnum():
                    print("<div>Grupa do dodania %s</div>" % newGroup)
                    command="insert into regions values('%s','%s','g');" % (addParent,newGroup)
                    try:
                        cur.execute(command)
                    except:
                        print("""&nbsp;&nbsp;&nbsp;<div style="color: red;">BŁĄD nie dodano %s,%s,g<div>""" %(addParent,newGroup))
                    else:
                        print("""&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dodano %s,%s,g<br>""" %(addParent,newGroup))
                else:
                    print("""<div style="color: red;">BŁĄD Nieprawidłowa nazwa "%s". <br>Nazwa może zawierać tylko cyfry i litery bez przerw.</div>""" % newGroup)
                        
        else:
            if form.getvalue('addParent') or form.getvalue('addChild') or form.getvalue('newGroup'):
                print("""<div style="color: red;">BŁĄD - musisz zazanczyć na górze grupę (zielone pole) i na dole obiekt</div>""")
            else:
                if  form.getvalue('firstTime')=='False':
                    print("""<div style="color: red;">BŁĄD - nic nie zaznaczono</div>""")
                
        
  
    
    regionCountersOld(region,1)
    
    print("""<form method = "post" action= "lvMap.py?service=%s">""" % service)
    
    print("""<pre>%s</pre>""" % mapa)
    print(""" <input type="hidden" name="sevice" value="%s"> """ % service)
    print(""" <input type="hidden" name="firstTime" value="False"> """) #ma zaznaczać, że to nie jest pierwsze otwarcie
    #print """<INPUT TYPE = "submit" VALUE = "Zatwierdź">""" 
  
    
    newTables=allTables()
    print("""Zaznacz powyżej czerwone pole aby usunąć obiekt, zielone pole i boxy przy obiektach poniżej, aby je dodać.<br>Usuwanie ma priorytet nad dodawaniem<br><br>""")
    print("<h2>Wykaz obiektów do dodania do zaznaczonej powyżej grupy.</h2>")
    print("<h3>Liczniki</h3>")
    #print """<p>Zaznacz przy liczniku box p dla wejścia lub wejścio-wyjscia, m dla wyjścia.<br>Jeżeli przy liczniku nie ma boksów, licznik już dodany.</p> """
    for x in newTables:
        counter=x.split("_")[0]
        month=x.split("_")[1]
        group=parent(counter,cur)
        if not group:
            gro="""p<input type="checkbox" name="addChild" value=%s>,  m<input type="checkbox" name="addChild" value=%s>""" % (counter+'+p',counter+'+m')
        else:
            gro="grupa: %s" % group
        print("%s, %s, ostatnie dane %s <br>" % (counter,gro,month))
    print("""<p>Zaznacz powyżej przy liczniku box p dla wejścia lub wejścio-wyjscia, m dla wyjścia.<br>Jeżeli przy liczniku nie ma boksów, licznik już dodany.</p> """)
    print("<h3>Nazwa nowej grupy do dodania:</h3>")
    print("""<input type="text" name="newGroup" maxlength="30" size="40"> """)
    print("""<br><br><INPUT TYPE = "submit" VALUE = "Zatwierdź">""")        
 

    try:
        cur.close()
        con.commit()
        con.close()
    except:
        print("<p>Nie udało się zamkniecie bazy danych</p>")
    else:
        print("<p>baza danych prawidłowo zamknięta</p>")
        print("""<p><a href="lvMap.py?service=%s">Odśwież</a></p>""" % service)
        print("""<a href="lvSu.py?service=%s">Powrót</a></form>  """ % service)
  

    print("</div><!--body1 w lvMap.py--></body></html>")


    #end

    #flogg.close()
firmName='Firma'        
parentList={firmName:''}
#keyDir="keyDir"
#session_seconds= leSe.sesTime
     
      
def main(logV,now,timestamp):
    global loggLevel
    vl=logV
    printLogg2(vl,0,"newLoggLevel= %d" % lvConf.loggLevel)
    loggLevel=lvConf.loggLevel
    #flogg=False
    keyName='noName'
    generateForm=True
    session_seconds= lvConf.sesTime
    #------------log tworzony zawsze od nowa
    '''if loggLevel:
        try:
            flogg=open(loggName, 'wb')
        except:
            #print "testtest3"
            os.system("echo 'I can't open logg file ' > error.txt")
            sys.exit("I can't open logg file")'''
    #vl=(flogg,loggLevel,timestamp,__version__)
    printLogg2(vl,0,"---Start----")
    #print "Content-Type: text/html\n"
    #odczytujemy ciastko, jeżeli to możliwe
    #ciastko może być z nieważnej sessji.
    cookie = http.cookies.SimpleCookie()
    string_cookie = os.environ.get('HTTP_COOKIE')
    printLogg2(vl,0, "string_cookie = %s" % string_cookie)
    sid=-1
    sessionKey=-1
    userN=False
    service=lvConf.mainService
    printLogg2(vl,0, "userN = %s" % userN)
           
  
#odczytujemy formularz, jeżeli to możliwe
    form = cgi.FieldStorage()
    printLogg2(vl,0,"1. form = %s" % str(form))
    if ("service" in form):
        service=(form["service"].value)
        printLogg2(vl,0,"service = %s" % service)
        #if (form.has_key("username")):
            
        #if string_cookie and not(form.has_key("username")):#dobra nazwa serwisu umożliwiała zalogowanie
        if string_cookie and not("username" in form):#dobra nazwa serwisu umożliwiała zalogowanie
            try:
                cookie.load(string_cookie)
            except:
                printLogg2(vl,0, "I can't load the cookie")
            else:
                printLogg2(vl,0,"string_cookie %s" % str(cookie))
                printLogg2(vl,0,"Session id from anywhere?= %s" % str(sid))
                try:
                    #sid = cookie['sess'].value
                    sid = int(cookie[service+'lvSu'].value)
                    printLogg2(vl,5,"Session id from cookie= %s" % str(sid))
                except:
                    printLogg2(vl,5,"I can't load session id 11 sid = %s" % str(sid))
                    generateForm=True
        #if (form.has_key("session_key")):
        printLogg2(vl,5,"Session id 3 = %s" % str(sid))
        #print userN
        '''
    else:
        lvSuSes.generate_form(inf="Nieznana nazwa serwisu",title='letronikVISITORS', header='Logowanie',panel='letronik')
        #service='letronik'
        sid=-1
        userN=False
        generateForm=False
'''

    printLogg2(vl,0,"Session id 1= %s" % str(sid))
    if sid>0:
        
        sessionKey= sid #form["session_key"].value.strip()
        keyName = os.path.join(keyDir,"key_%s.txt" % (sessionKey))
        userN=lvSuSes.fetch_username(keyName)
        #try:
            #service=userN.split('@')[1]
        #except:
            #service='main'
        #sledz(logV, 9,"sessionKey = %s" % str(sessionKey))
        #sledz(logV, 9,"userN = %s" % userN)
    printLogg2(vl,0,"Session user 1= %s" % userN)
    if userN: #jeżeli isnieje plik sessji
                
        try:
            printLogg2(vl,0,'20 service = %s' % service)
            sys.path.append(service)
            import lvCf #plik konfiguracji
            printLogg2(vl,0,'21')
            import leSe
            printLogg2(vl,0,'22')
        except:
            lvSuSes.generate_form("serwis nie istnieje, zła nazwa")
        else:
            loggLevel=lvCf.loggLevel
            printLogg2(vl,0,'23 loggLevel = %s,sessionKey= %s' % (str(loggLevel),str(sessionKey)))
            #region=lvCf.region
            #firmName=lvCf.firmName
            #firmCode=lvCf.firmCode
            #direction = lvCf.direction
            #session_seconds= lvConf.sesTime

            ts=lvSuSes.fileAge("%s/key_%s.txt" % (keyDir,sessionKey),'s')
            printLogg2(vl,8,"Session age = %d" % ts)
        
            #print "form has key session_key = ->%s<- <br>" % sessionKey
            if ("" in form or ts>session_seconds):
                #print "form has key logout <br>"
                lvSuSes.delete_session(keyName)
                if ts>session_seconds:
                    lvSuSes.generate_form("Przekroczyłeś czas bezczynności.<br> %dsek &gt; MAX=%d <br>Możesz zalogować się ponownie."% (ts ,session_seconds))
                else:
                    lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.",panel=service)
            else:#ważma sessja
                result = lvSuSes.testFirm(service,userN , '','')
                #lvSuSes.display_page(userN,result,ts,cookie,sessionKey)
                core(logV,form,lvCf,leSe,userN,result,ts,cookie,sessionKey,service,vl)
                lvSuSes.write_session(userN,keyName)


        


    elif ("action" in form and "username" in form \
    and "password" in form and "panel" in form):
        if (form["action"].value == "display"):
            userN=form["username"].value
            service=form["panel"].value
            printLogg2(vl,0,"1. Service Name %s" % service)


                
            
            try:
                sys.path.append(service)
                import lvCf #plik konfiguracji
                import leSe
            except:
                lvSuSes.generate_form("serwis nie istnieje, zła nazwa")
            else:
                printLogg2(vl,0,"1. lvCf.firname %s lvCf.loggLevel = %d, vl = %s" % (lvCf.firmName,lvCf.loggLevel,vl))
                loggLevel=lvCf.loggLevel
                #logV=(__version__, loggLevel, timestamp,logName)
                vl=(vl[0],loggLevel,vl[2],vl[3])
                printLogg2(vl,0,"3. vl = %s" % str(vl))
                #region=lvCf.region
                #firmName=lvCf.firmName
                firmCode=lvCf.firmCode
                #direction = lvCf.direction
                #session_seconds= leSe.sesTime
                headerName=">%s<" % service
                siteTitle=">%s<" % service
                """
                try:
                    headerName=leSe.headerName
                    siteTitle=leSe.siteTitle
                except:
                    pass"""

                
                printLogg2(vl,6,"4. userN from form = %s,firmCode= %s" % (userN,firmCode))
                 
                result = lvSuSes.testFirm(service,userN , form["password"].value)
                printLogg2(vl,6,"result from form = %s" % str(result))
                if result[:3] != 'err':
                    ts=0
                    sessionKey=lvSuSes.create_session(userN,keyDir)
                    #lvSuSes.display_page(userN,result,ts,cookie, sessionKey)
                    printLogg2(vl,6,"before core: userN from form = %s,firmCode= %s, vl=%s" % (userN,firmCode,str(vl)))
                    printLogg2(vl,6,"logV=%s,form %s,lvCf=%s,leSe=%s, userN=%s,result=%s,ts=%s,cookie=%s,sessionKey=%s,service=%s,vl=%s" %\
                                (str(logV),str(form),str(lvCf),str(leSe), str(userN),str(result),str(ts),str(cookie),\
                               str(sessionKey),str(service),str(vl)))
                    core(logV,form,lvCf,leSe, userN,result,ts,cookie,sessionKey,service,vl)
                    #core(form, userN,result,ts, sessionKey)
                else:
                    lvSuSes.generate_form("Podałeś złe dane")
        else:
            lvSuSes.generate_form("Podałeś złe dane",panel='error')                    
    elif ("action" in form and not ("username" in form \
    and "password" in form and "panel" in form)):
        lvSuSes.generate_form("Brakuje danych")

    else:
        if generateForm:
            userDe=''
            panelDe=service
            if "panelD" in form:
                panelDe=form["panelD"].value
            if "userD" in form:
                userDe=form["userD"].value                
            sledz(logV, 9,"Content-Type: text/html\n\nsessionKey = %s" % str(sessionKey))
            sledz(logV, 9,"userN = %s" % userN)
            if loggLevel>0:
                #przy nowym logowaniu loggLevel zawsze równy 0
                lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.<br>\
Nie znaleziono użytkownika sesji U=%s C=%s K=%s" % (userN,string_cookie,keyName),panelD=panelDe,userD=userDe)
            else:
              
                lvSuSes.generate_form("Dziękujemy za skorzystanie z serwisu.<br> Zapraszamy ponownie.",panelD=panelDe,userD=userDe)
    '''if loggLevel:
        flogg.close()'''

script=os.path.basename(sys.argv[0])     
sys.stderr = sys.stdout

#now=datetime.now()
#timestamp=now.strftime("%Y%m%d %H:%M:%S ")

main(logV,now,timestamp)


    
