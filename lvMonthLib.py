#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from openpyxl import Workbook
from lvCommonLib import greens, blues, sledz,regionParents2,byMonths,printLogg,countersInRegion,pHelp,dayTimes
import os
from datetime import datetime, timedelta
ver='202102230945'
'''
lvMonthLib
202102230945
usunięcie with
202002280921
do cpf.write dodanie encode('utf-8')
201910311621
działa kalibracja
def monthData(startDT,stopDT,direction,logV,regChecked,con,flagChecked,parentList,lvCf,kolorDict,equalization):
def monthsMake(service,startDT,stopDT,direction,logV,regChecked,con,flagChecked,parentList,lvCf,kolorDict,equalization):
201910311435
    try:
        equal=equalization[namePrefix]
    except:
        equal=1.0 
201910311246

def monthsMake(service,startDT,stopDT,direction,logV,regChecked,con,flagChecked,parentList,lvCf,kolorDict,equalization)

'''

#ddwm dane do wykresu miesięcznego start
def monthData(startDT,stopDT,direction,logV,regChecked,con,flagChecked,parentList,lvCf,kolorDict,equalization):
    sledz(logV,8,"<b>Generowanie danych do tablicy miesięcznej</b>")
    sledz(logV,6, " regChecked do rekordów "+str(regChecked))
    counterList={}

    maxSumD=1#prosty sposób aby nie dzielić przez zero
    totalD=[0,0,0,0]
    totalDI=[0,0,0,0]
    totalDO=[0,0,0,0]
    dayTable=[]#zebrać wszystkie dane, wyskalować a następnie wyświetlać
    '''[0]data,[1],nawa obiektu,[2]znak,[3]suma,[4]F, [5]C, [6]S'''
    regionToCheckD=''# do przekazania w polu ukrytym do sprawdzania chechBoxów



    #with con:
    try:

        #cur = con.cursor()
        cur = con.cursor(buffered=True)
        if flagChecked==True:#len(regChecked)>0:
            #może to trzeba zwrócić? 20170605
        #if 1==0:
            rebiekt=regChecked
            sledz(logV, 6, 'rebiekt ' + str( rebiekt))

        # sortowanie rebiektów po rodzicach       
            parentList = regionParents2(logV,rebiekt, parentList, cur)#tworzy global parentList
            sledz(logV, 9,'parentList = '+ str(parentList))
            sortList=[]
            for x in rebiekt:
                sortList.append(('%s%s' %(parentList[x[0]],x[0]),x))
            sledz(logV, 9,'sortList = '+  str(sortList))
            sortList.sort()
            rebiekt=[]
            for x in sortList:
                rebiekt.append(x[1])
            sledz(logV,6, 'rebiekt sortowany ' + str( rebiekt))
            sledz(logV,5,"<b>counterList</b>")
            counterList={}        
            #sledz(loggLevel, 5,counterList)

            
            #for nazwa, tresc in counterList.items():
            for nazwa in rebiekt:
                sledz(logV,5,"<b>nazwa = </b>" + str(nazwa))
                if nazwa[1]!= 'g':#czyli nazwa jest licznikiem, m lup p
                    tresc=[nazwa]
                    #tresc jest listą dzieci
                else:#nazwa jest grupą liczników
                    #counterLs=[]
                    counterLs=countersInRegion(nazwa[0],cur,[])
                    tresc=counterLs
                sledz(logV, 9, "treść"+ str(tresc))
                counterList[nazwa]=tresc
                '''nazwa - nazwa obiektu, tresc - wykaz licznikow'''
            sledz(logV, 9, "<br> couterlist: "+str(counterList))

       # else:
            #pHelp(0,"<b>Nie wybrano obiektów do analizy.</b>")
            regionList=rebiekt
            sledz(logV, 5,"<br>dayTimes=" + str(dayTimes(startDT,stopDT))+"<br>")
            #20170206 rowspanNr
            '''
            rowspanNr=0
            for nazwa in rebiekt:
                rowspanNr+=len(counterList[nazwa])
                '''
                #sledz(loggLevel, 6,'nazwa = %s, len=%d' % (str(counterList[nazwa]),len(counterList[nazwa])))
            #sledz(loggLevel, 6,'rowspanNr = %d' % rowspanNr)
            
            #for dayx in dayTimes(startDT,stopDT):
            for  monthx  in byMonths(startDT,stopDT):
                """
---- 7 ----> Tablice miesięczne
---- 7 ----> ['1701', '010000', '312401']
---- 7 ----> ['1702', '000000', '312401']
---- 7 ----> ['1703', '000000', '062011'] """
                    #print monthX[0],"<br>"
                    #for dayX in monthX[1]:
                sledz(logV,6,'monthx = %s' % str(monthx))
                if True:
                    #startDH="%s0000" % dayX
                    #stopDH="%s2400" % dayX
                    #print "<br> start = %s stop = %s" % (startDH, stopDH)
                    for nazwa in rebiekt:
                        #nazwa((obiekt,sn),kolor)
                        sledz(logV, 9,'nazwa = %s' % str(nazwa))
                        sledz(logV, 9,'parentList = %s' % str(parentList[nazwa[0]]))
                        sledz(logV, 9,'tresc = %s' % str(tresc))
                        sledz(logV, 9, 'regionList = %s' % str(regionList))
                        tresc=counterList[nazwa]
                        sledz(logV, 6,"tresc = %s" % tresc)
                        '''nazwa - nazwa obiektu, tresc - wykaz licznikow'''                
                    #for nazwa, tresc in counterList.items():#iteracja po obiektach
                        '''nazwa - nazwa obiektu, tresc - wykaz licznikow'''
                        if len(tresc)>0:
                            #pLogg(vl,8, "nazwa - %s tresc - %s<br>" % (nazwa, tresc))
                            #print nazwa
                            regDayRow=[monthx[0]+monthx[1][:2],'%s' % (nazwa[0]),nazwa[1],kolorDict[nazwa[0]],0,0,0,0]
                            regDayRowI=[monthx[0]+monthx[1][:2],'%s' % (nazwa[0]),nazwa[1]+'-p',kolorDict[nazwa[0]],0,0,0,0]
                            regDayRowO=[monthx[0]+monthx[1][:2],'%s' % (nazwa[0]),nazwa[1]+'-m',kolorDict[nazwa[0]],0,0,0,0]
                            #print "<br>",regDayRow
                            #regRowDif=[nazwa[0],nazwa[1],0,0,0,0]
                            #regionToCheck+=nazwa[0]+','+nazwa[1]+';'
                            sledz(logV, 5,"kalibracja %s" % str(equalization))
                            for x in tresc:#iteracja po licznikach
                                namePrefix=x[0]
                                
                                #suma=[0,0,0,0]
                                #dataRow=[namePrefix,x[1],0,0,0,0]
                                tabName="%s_%s%s" %(namePrefix,lvCf.centPref,monthx[0])
                                #print tabName

                                try:
                                    equal=equalization[namePrefix]
                                except:
                                    equal=1.0
                                sledz(logV, 5,"kalibracja %f" % equal)
                                sledz(logV, 6,"tabName = %s" % tabName)
                                #pLogg(vl,8, "tabName = %s" % tabName)

                                command="select sum(ep),sum(ec),sum(es) from %s where time>='%s' and time <='%s';" \
                                         % (tabName,monthx[1], monthx[2])
                                command1="select sum(count) from %s where time>='%s' and time <='%s';" \
                                 % (tabName,monthx[1], monthx[2])
                                #command2="select time,count from %s where time>='%s' and time <='%s';" \
                                #% (tabName,startTime, stopTime)                

                                    
                                #pLogg(vl,6,command+'<br>'+command1)
                                try:
                                    cur.execute(command)
                                    sums=cur.fetchone()
                                    cur.execute(command1)
                                    suma=cur.fetchone()[0]
                                    suma=str(int(round(int(suma)*equal)))

                                except:
                                    sums=[0,1,1]
                                    suma=[0]
                                #print "wzrost=%s, err_photo=%s, err_comm=%s, err_sum%s"% (suma,sums[0],sums[1],sums[2])
                                   
                                #pLogg(vl,8, "wzrost=%s, err_photo=%s, err_comm=%s, err_sum%s"% (suma,sums[0],sums[1],sums[2]))
                                sledz(logV, 8,tabName+': '+str(suma)+str(sums))
                                #nit=0
                                isums=[0,0,0,0]
#140127-03
                                for it in range(4):
                                    if it==0:
                                        try:
                                            isums[it]=int(suma)
                                        except:
                                            pass
                                    else:
                                        try:
                                            isums[it]=int(sums[it-1])
                                        except:
                                            pass
                                    #total[it]+=isums[it]
                                if x[1]=='p'and (direction=='inside' or direction=='in') : # jeżeli wejśicia , dodajemy wejścia
                                    regDayRow[4]+=isums[0]
                                    totalD[0]+=isums[0]
                                if x[1]=='m' and direction=='inside': # jeżeli wyjścia , dodajemy wejścia
                                    regDayRow[4]-=isums[0]
                                    totalD[0]-=isums[0]
                                if x[1]=='p'and direction=='inAout' : # jeżeli wejśicia , dodajemy wejścia
                                    regDayRowI[4]+=isums[0]
                                    regDayRow[4]=regDayRowI[4]
                                    totalDI[0]+=isums[0]
                                if x[1]=='m' and direction=='inAout': # jeżeli wyjścia , dodajemy wejścia
                                    regDayRowO[4]-=isums[0]
                                    regDayRow[4]=regDayRowO[4]
                                    totalDO[0]-=isums[0]                        
                                    
                                for n in [1,2,3]:
                                    if x[1]!='g': # jeżeli wejśicia , dodajemy wejścia
                                        if direction=='inside' or (direction=='in' and x[1]=='p'):
                                            regDayRow[n+4]+=isums[n]
                                            totalD[n]+=isums[n]
                                        if direction=='inAout':
                                            if x[1]=='p':
                                                regDayRowI[n+4]+=isums[n]
                                                totalDI[n]+=isums[n]
                                            if x[1]=='m':
                                                    regDayRowO[n+4]+=isums[n]
                                                    totalDO[n]+=isums[n]                                      
                                                
                                if abs(regDayRow[4])>maxSumD:
                                    maxSumD = abs(regDayRow[4])
                                if abs(regDayRowI[4])>maxSumD:
                                    maxSumD = abs(regDayRowI[4])
                                if abs(regDayRowO[4])>maxSumD:
                                    maxSumD = abs(regDayRowO[4])
                            if direction!='inAout':
                                dayTable.append(regDayRow)
                            else:
                                if nazwa[1]=='g':
                                    dayTable.append(regDayRowI)
                                    dayTable.append(regDayRowO)
                                if nazwa[1]=='p':
                                    dayTable.append(regDayRowI)
                                    #dayTable.append(regDayRowO)
                                if nazwa[1]=='m':
                                    #dayTable.append(regDayRowI)
                                    dayTable.append(regDayRowO)                    
                            
            #dayTable.sort()
            if direction=='in':
                dayTable.append([('',"RAZEM"),'&nbsp;', totalD[0],totalD[1],totalD[2], totalD[3]])
            elif direction=='inside':
                dayTable.append([('',"RAZEM-R"),'&nbsp;', totalD[0],totalD[1],totalD[2], totalD[3]])
            else:
                dayTable.append([('',"RAZEM-P"),'&nbsp;', totalDI[0],totalDI[1],totalDI[2], totalDI[3]])
                dayTable.append([('',"RAZEM-M"),'&nbsp;', totalDO[0],totalDO[1],totalDO[2], totalDO[3]])
            sledz(logV, 8, dayTable)
            sledz(logV, 8,"Maksymalna suma dzienna=%d" % maxSumD)
        else:
            pHelp(0,'''<b>Nie wybrano obiektów do analizy.</b><br>
Zaznacz w tabeli checkboksy<br>
przy opowiednich obiektach.
''')
    except:
        print("<h1>Błąd bazy danych 002</h1>")
        try:
            con.close()
        except:
            print("<h1>Błąd zamknięcia bazy danych 002</h1>")
            
    finally:
        con.close()            
            
    return(dayTable,maxSumD)





#-------malowanie tablicy miesięcy start
def monthsDrawCB(dataTable1, maxSum1,objNo, loggLevel,kolorDict):
    #global kolorDict
    #objNo - ilość obiektów, do sklejania razem wierszy obiektów z jednego dnia
            
    print('''<table style="font-size: 12px;" border="1" cellpadding="0"
    cellspacing="0">
    <tbody>
    <tr>
    <th>YYMM</th>
    <th align=center>B</th>    
    <th>Obiekt</th>
    <th>Sn</th>
 
    <th  align="center">Wzrost</th>
    <th>Graf</th>
    <th>F</th>
    <th>K</th>
    <th>S</th>
  

    </tr>''')
    rowNo=0#licznik wierszy do sklejania wierszy

    for item in dataTable1:

        if len(item)>6:#item[0][1]!="RAZEM":
            if item[5]+item[6]+item[7]>0:
                xcolor='bgcolor=red'
            else:
                xcolor=''            

            print('<tr ')
            #print "style=\"font-weight: bold;\""
            if (rowNo // objNo) % 2==0:
                print(''' bgcolor="#d0d0d0"''')
            print('>')
            rowNo+=1
            rowNoM=rowNo % objNo


            box='''<input type="checkbox" name="r%s" value="on" >''' % (item[0])#albo nr wiersza
            if rowNoM==1 or objNo==1:
                print('''
<td align="left" rowspan=%d>&nbsp;%s</td>
<td align="center" rowspan=%d>%s</td>''' % (objNo,item[0][0:4],objNo, box))
            print('''
<td> %s </td>
<td align="center">%s</td>
<td align="right">%3d&nbsp;</td>
'''  % (item[1],item[2],item[4]))
            #sledz(logV, 1,greens[kolorDict[item[1]]])
            #malowanie słupka
            #długość  i kolor
            width=item[4]*200/maxSum1
            if width<0:
                width= -1*width
                colour=blues[kolorDict[item[1]]]
                framecolour="red"
            else:
                colour=greens[kolorDict[item[1]]]
                framecolour="green"
            print('''
<td align="left" width="200">''')
            if item[4] != 0:
                print('''
    <table border="2" cellpadding="0" cellspacing="0" bordercolor="%s">
    <tbody>
    <tr>''' % framecolour)
                print('''
    <td style="height: 7px; width: %dpx; font-size: 8px;"
    bgcolor="%s">'''% (width, colour))
                print('''
    </td>
    </tr>
    </tbody>
    </table></td>''')
            else:
                print('&nbsp;')

            
            print('''
<td %s >&nbsp;%d&nbsp;</td>
<td %s >&nbsp;%d&nbsp;</td>
<td %s >&nbsp;%d&nbsp;</td>

</tr>''' % (xcolor,item[5],xcolor,item[6],xcolor,item[7]))
        
        else:
            if 1:#loggLevel>0:
                print('''
<tr style="font-weight: bold;">
<td colspan=3 align=right>%s</td>
<td colspan=2 align=right>%d&nbsp;</td>
<td>&nbsp;</td>''' % (item[0][1],item[2]))


                print('''


<td %s >&nbsp;%d&nbsp;</td>
<td %s >&nbsp;%d&nbsp;</td>
<td %s >&nbsp;%d&nbsp;</td>

</tr>''' % (xcolor,item[3],xcolor,item[4],xcolor,item[5]))
            
    print('''

    </tbody>
    </table>
    ''')
#-------malowanie tablicy miesięcy stop

def monthsMake(service,startDT,stopDT,direction,logV,regChecked,con,flagChecked,parentList,lvCf,kolorDict,equalization):
    loggLevel=logV[1]
    sledz(logV,8,"navi == 'months'\n")
    sledz(logV, 8,"\n\n\n\nstartDT=%s,stopDT=%s,direction=%s,logV=%s,\nregChecked=%s,con=%s,flagChecked=%s,\nparentList=%s\n"\
          % (str(startDT),str(stopDT),str(direction),str(logV),str(regChecked),str(con),str(flagChecked),str(parentList)))
    monthD=monthData(startDT,stopDT,direction,logV,regChecked,con,flagChecked,parentList,lvCf,kolorDict,equalization)
    sledz(logV, 8,"\n\n-----K--------monthD=%s" % str(monthD))
    dayTable=monthD[0]
    maxSumD=monthD[1]
    rowspanNr=0

    
    #tworzenie csv i xlsx
    wb = Workbook()
    ws = wb.active
    csvFile=os.path.join(service,'months.csv')
    xlsxFile=os.path.join(service,'months.xlsx')
    cfp=open(csvFile, 'wb')
    cfp.write('Data,Nazwa,Znak,Ilość,F,K,S\n'.encode('utf-8'))
    ws.append(['Data','Nazwa','Znak','Ilość','F','K','S'])
    #cfp.write(str(dataTable))
    for row in dayTable:
        sledz(logV, 8,row)
        if row[0]==dayTable[0][0]:
            rowspanNr+=1
            
        if len(row)==8:
            ws.append(['%s%s' % (row[0][0:2],row[0][2:4]),row[1],row[2],row[4],row[5],row[6],row[7]])
            cfp.write(('%s%s, %s, %s, %s ,%s ,%s ,%s\n' % \
(row[0][0:2],row[0][2:4],row[1],row[2],row[4],row[5],row[6],row[7])).encode('utf-8'))

        if len(row)==6:
            ws.append(['RAZEM',row[0][1],'',row[2],row[3],row[4],row[5]])
            cfp.write(('RAZEM,%s,, %s, %s, %s, %s\n' % \
                      (row[0][1],row[2],row[3],row[4],row[5])).encode('utf-8'))
    cfp.close()
    wb.save(xlsxFile)

    print('''
<div style="page-break-before: always; font-size: 14px; font-weight: bold ">\
Odwiedzalność miesięczna.\
<a href="download.py?path=%s&filename=months.csv" target="_blank">CSV</a>&nbsp;\
<a href="download.py?path=%s&filename=months.xlsx" target="_blank">XLSX</a></div>''' % (service, service))
    #span=len(rebiekt)
    #if direction=="inAout":
        #span=rowspanNr
        #span=1
    

    #daysDrawCB(dayTable, maxSumD,span,loggLevel)
    monthsDrawCB(dayTable, maxSumD,rowspanNr,loggLevel,kolorDict)
    sledz(logV, 9, "Wykonano monthsDrawCB(dayTable, maxSumD,rowspanNr,loggLevel,kolorDict")
    sledz(logV, 9,"dayTable")
    sledz(logV, 9,dayTable)
#------------wyświetlanie miesięcy stop



#ddwm dane do wykresu miesięcznego stop
