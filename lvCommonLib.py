#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
ver='201711_1751'
'''
201711_1751
tmp/zelVC_lv.logg
20170706_1540
poprawianie pHelp
start 20170531
funkcje wspólne dla lv
logV=(__version__, loggLevel, timestamp)
'''


greens={9:"#008000",5:"#00C000",1:"#00ff00",10:"#80FF80",6:"#C0FFC0",2:"#E0FFc0",11:"#ffffc0",7:"#ffff80",3:"#ffFF00",12:"#ffe000",8:"#ffb000",4:"#ff6000"}   
blues= {9:"#000080",5:"#0000C0",1:"#0000ff",10:"#8080ff",6:"#C0C0ff",2:"#E0c0ff",11:"#ffc0ff",7:"#ff80ff",3:"#ff00ff",12:"#ff00e0",8:"#ff00b0",4:"#ff0060"}


def sledz(logV,level,tresc):
    if level <= logV[1]:
        print("<br>--level %d ----> " % level, tresc)

def printLogg(logV,a=0,tekst='wpis domyśny do loggu'):
    '''printLogg - zapisywanie do loggu.'''
    
    if logV[1] >= a :
        flogg=open('tmp/lv.logg','a')
        flogg.write(logV[2]+' '+logV[0]+' '+str(tekst)+'\n')
        flogg.close()
def printLogg2(logV,a=0,tekst='wpis domyśny do loggu'):
    '''printLogg - zapisywanie do loggu.'''
    
    if logV[1] >= a :
        text = (logV[2]+' '+logV[0]+' '+(tekst)+'\n')
        #print('<br>')
        #print(text)
        #print('<br>')
        flogg=open(logV[3],'ab')
        flogg.write((logV[2]+' '+logV[0]+' '+(tekst)+'\n').encode())
        flogg.close()        
helpLevel=0       
def pHelp(level,helpLevel,tresc):
    '''
printHelp
'''
    if level<= helpLevel:
        print("<table style=\"font-size: 11px;\"><tr><td bgcolor=yellow>&nbsp;%s&nbsp;</td></tr></table> " %  tresc)   
#countersInRegion start
#counterLs - wykaz liczników w regione jest konieczne do rekurencji, musi zostać.
now=''
#counterLs=[]
def countersInRegion(region,cur,counterLs):
    #global counterLs
    command ="select child, sign from regions where parent='%s';" % region
    #print command
    cur.execute(command)
    lista=cur.fetchall()
    #print lista
    for x in lista:
        child=x[0]
        sign=x[1]
        #print child, sign
        if sign!='g':
            counterLs.append((child,sign))
        else:
            countersLs=countersInRegion(child,cur,counterLs)
    return counterLs

#countersInRegion stop
def byMonths2(sy,sm,sdd, shh, shm,fy,fm,fdd, fhh, fhm):
    '''Wyznacza tablice w bazie i okesy w ramach 1 roku'''

    #print fdd,fm,fy
    if sy==fy:
        if sm==fm:
            monthList=[['%02d%02d' % (sy,sm),'%02d%02d%02d' % (sdd,shh,shm),'%02d%02d%02d' % (fdd,fhh,fhm)]]
        else:
            monthList=[['%02d%02d' % (sy,sm),'%02d%02d%02d' % (sdd,shh,shm),'312401']]
            bm=sm+1
            while bm<fm:
                monthList.append(['%02d%02d' % (sy,bm),'000000','312401' ])
                bm+=1
                
            monthList.append(['%02d%02d' % (fy,fm),'000000','%02d%02d%02d' % (fdd,fhh,fhm)])
    return monthList
        
               
def byMonths(sd,fd):
    '''Wyznacza tablice w bazie i okesy przez lata
sd=startd datetime
sh='07:15'
fd=stop datetime
fh='20:27'

'''
    sy=sd.year%100
    sm=sd.month
    sdd=sd.day
    shh=sd.hour
    shm=sd.minute
    fy=fd.year%100
    fm=fd.month
    fdd=fd.day
    fhh=fd.hour
    fhm=fd.minute
    if fhh==23 and fhm==59:
        fhh=24
        fhm=00

    if sy==fy:
        return byMonths2(sy,sm,sdd, shh, shm,fy,fm,fdd, fhh, fhm)
        
    else:
        monthList=byMonths2(sy,sm,sdd, shh, shm,sy,12,31, 24, 0o1)
        by=sy+1
        while by<fy:
            monthList+=byMonths2(by,0o1,00, 00, 00,by,12,31, 24, 0o1)
            by+=1
            
        monthList+=byMonths2(fy,0o1,00, 00, 00,fy,fm,fdd, fhh, fhm)
        return monthList
        
def regionParents2(logV,lista,parentList,cur):
    '''
parentList to słownik kreowany nastęþująco parentList={firmName:''}
wyraźnie parentList jest wynikiem działania skryptu, dlatego nie musi byc
w parametrach wywołania, tylko w return
'''
    sledz(logV,8,"regionParents2(logV,lista=%s,cur)" % str(lista))
    #global parentList, cur
    for x in lista:
        printLogg(logV,1, "21. regionParents2(lista)")
        command ="select parent from regions where child='%s' and sign='%s';" % (x[0],x[1])
        printLogg(logV,1, command)
        cur.execute(command)
        parentV=cur.fetchone()
        printLogg(logV,1, "22. parentV = %s" % str(parentV))
        #print parentV
        #sys.exit(1)
        if parentV:   
            parentList[x[0]]=parentV[0]
            printLogg(logV,1,parentList[x[0]])
            while parentV:
                command ="select parent from regions where child='%s' ;" % parentV[0]
                cur.execute(command)
                parentV=cur.fetchone()
                if parentV:
                    parentList[x[0]]="%s/%s" % (parentV[0],parentList[x[0]])
            parentList[x[0]]="%s/" % (parentList[x[0]])
                    
                
        else:
            parentList[x[0]]=''
    return parentList


def dayTimes(st,ft):
    '''listę krotek dziennych (yymm,ddhhMM,ddhhMM) - '''
    timesLs=[]


    item=(st.strftime('%y%m'),[])
    if st.day==ft.day and st.month==ft.month and st.year==ft.year:
        if ft.strftime('%H%M')=='2359':
            finish=ft.strftime('%d2400')
        else:
            finish=ft.strftime('%d%H%M')
        timesLs.append((st.strftime('%y%m'),st.strftime('%d%H%M'),finish))
    else:
        bt=st+timedelta(1)
        #print bt
        timesLs.append((st.strftime('%y%m'),st.strftime('%d%H%M'),st.strftime('%d2400')))                
        while bt<ft.replace(hour=0, minute=0,second=0,microsecond=0):
            timesLs.append((bt.strftime('%y%m'),bt.strftime('%d0000'),bt.strftime('%d2400')))
            bt+=timedelta(1)
        if ft.strftime('%H%M')=='2359':
            finish=ft.strftime('%d2400')
        else:
            finish=ft.strftime('%d%H%M')
        timesLs.append((ft.strftime('%y%m'),ft.strftime('%d0000'),finish))            

                
    return timesLs
